#!/usr/bin/python3

import json
import requests


class AnimalApi:
    def __init__(self):
        self.endpoint = "https://explorer.natureserve.org/"

    def get_animal_by_name(self, name):
        code = self.search_animals([name], {})[0].get("uniqueId")
        return self.get_animal_by_code(code)

    def get_animal_by_code(self, code):
        r = requests.get(f"{self.endpoint}api/data/taxon/{code}")
        return r.json()

    def search_animals(self, keywords, filters):
        headers = {"Content-Type": "application/json"}
        body = {
            "criteriaType": "species",
            "statusCriteria": [{"paramType": "globalRank", "globalRank": "G1"}],
            "pagingOptions": {"recordsPerPage": 1000},
        }
        r = requests.post(
            f"{self.endpoint}api/data/speciesSearch",
            headers=headers,
            data=json.dumps(body),
        )
        return r.json().get("results")


if __name__ == "__main__":
    api = AnimalApi()
    print("Getting animal by name dog")
    print(api.get_animal_by_name("elephant"))
    print([animal.get("primaryCommonName") for animal in api.search_animals([], {})])
