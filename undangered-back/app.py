from flask import Flask, request, jsonify
from flask_cors import CORS
from settings import DB_URL

app = Flask(__name__)
cors = CORS(app)


app.config["SQLALCHEMY_DATABASE_URI"] = DB_URL
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

from connect import (
    get_num_commits,
    get_individual_commits,
    get_num_issues,
    get_issue_states,
)
from db import AnimalQuery, NationQuery, OrganizationQuery

"""
Get commit count
"""


@app.route("/api/commitCount")
def api_commits():
    try:
        num_commits = get_num_commits()
        return jsonify(num_commits), 200
    except:
        return "", 204


"""
Get detailed commit data
"""


@app.route("/api/commitData")
def api_commitdata():
    try:
        commit_data = get_individual_commits()
        return jsonify(commit_data), 200
    except:
        return "", 204


"""
Get issue count
"""


@app.route("/api/issueCount")
def api_issue_count():
    try:
        num_issues = get_num_issues()
        return jsonify(num_issues), 200
    except:
        return "", 204


"""
Get detailed issue data
"""


@app.route("/api/issueStates")
def api_issue_states():
    try:
        issue_states = get_issue_states()
        return jsonify(issue_states), 200
    except:
        return "", 204


"""
Animal search by name endpoint
"""


@app.route("/api/animalInfo/<name>")
def api_animal_info(name):
    api = AnimalQuery()
    try:
        animal_info = api.get_animal(name)
        return jsonify(animal_info), 200
    except:
        return "", 204


"""
Animal retrieval endpoint
"""


@app.route("/api/animalSearch")
def api_animal_search():
    api = AnimalQuery()
    try:
        animals = api.search_animals()
        return jsonify(animals), 200
    except:
        return "", 204


"""
Animal search by country endpoint
"""


@app.route("/api/animalsByCountry/<name>")
def api_animal_search_by_country(name):
    api = AnimalQuery()
    try:
        animals = api.search_animals_by_country(name)
        return jsonify(animals), 200
    except:
        return "", 204


"""
Animal search by organization endpoint
"""


@app.route("/api/animalsByOrganization/<name>")
def api_animal_search_by_org(name):
    api = AnimalQuery()
    try:
        animals = api.search_animals_by_org(name)
        return jsonify(animals), 200
    except:
        return "", 204


"""
Animal search by id endpoint
"""


@app.route("/api/animalsById/<id>")
def api_animal_search_by_id(id):
    api = AnimalQuery()
    try:
        animal = api.search_animals_by_id(int(id))
        return jsonify(animal), 200
    except:
        return "", 204


"""
Country count by animal endpoint
"""


@app.route("/api/animalCountByCountry")
def api_animal_count_by_country():
    api = AnimalQuery()
    try:
        animal = api.get_country_count_by_animals()
        return jsonify(animal), 200
    except:
        return "", 204


"""
Nation search by name endpoint
"""


@app.route("/api/nationInfo/<name>")
def api_nation_info(name):
    api = NationQuery()
    try:
        nation_info = api.get_nation(name)
        return jsonify(nation_info), 200
    except:
        return "", 204


"""
Nation retrieval endpoint
"""


@app.route("/api/nationSearch/")
def api_nation_search():
    api = NationQuery()
    try:
        nations = api.search_nations()
        return jsonify(nations), 200
    except:
        return "", 204


"""
Nation search by id endpoint
"""


@app.route("/api/nationsById/<id>")
def api_nation_search_by_id(id):
    api = NationQuery()
    try:
        nation = api.search_nations_by_id(int(id))
        return jsonify(nation), 200
    except:
        return "", 204


"""
Organization search by name endpoint
"""


@app.route("/api/organizationInfo/<name>")
def api_organization_info(name):
    api = OrganizationQuery()
    try:
        org_info = api.get_organization(name)
        return jsonify(org_info), 200
    except:
        return "", 204


"""
Organization retrieval endpoint
"""


@app.route("/api/organizationSearch/")
def api_organization_search():
    api = OrganizationQuery()
    try:
        orgs = api.search_organizations()
        return jsonify(orgs), 200
    except:
        return "", 204


"""
Organization search by country endpoint
"""


@app.route("/api/organizationsByCountry/<name>")
def api_org_search_by_country(name):
    api = OrganizationQuery()
    try:
        animals = api.search_orgs_by_country(name)
        return jsonify(animals), 200
    except:
        return "", 204


"""
Organization search by id endpoint
"""


@app.route("/api/organizationsById/<id>")
def api_org_search_by_id(id):
    api = OrganizationQuery()
    try:
        orgs = api.search_orgs_by_id(int(id))
        return jsonify(orgs), 200
    except:
        return "", 204


"""
General search endpoint
"""


@app.route("/api/search/<term>")
def api_general_search(term):
    terms = term.lower().split()
    apis = {
        "animals": AnimalQuery(),
        "nations": NationQuery(),
        "organizations": OrganizationQuery(),
    }
    stuff = {"animals": [], "nations": [], "organizations": []}

    for name, api in apis.items():
        for result in api.get_all():
            if any(
                term in str(value).lower()
                for value in result.values()
                for term in terms
            ):
                stuff[name].append(result)

    return jsonify(stuff), 200


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
