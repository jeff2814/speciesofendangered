from models import db

"""
Model of an country in the db, with id as the primary key
"""


class Country(db.Model):
    __tablename__ = "country"
    code = db.Column(db.String, unique=True, nullable=False)
    name = db.Column(db.String)
    capital = db.Column(db.String)
    region = db.Column(db.String)
    latitude = db.Column(db.String)
    longitude = db.Column(db.String)
    primary_image = db.Column(db.String)
    secondary_image = db.Column(db.String)
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
