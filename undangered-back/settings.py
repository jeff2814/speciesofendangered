import json

settings_file = "db.json"
with open(settings_file, "r") as f:
    settings = json.load(f)

DB_URL = (
    f'postgresql://{settings["username"]}:{settings["password"]}@'
    f'{settings["endpoint"]}:{settings["port"]}/{settings["db_name"]}'
)
