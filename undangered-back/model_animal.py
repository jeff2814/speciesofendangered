from models import db

"""
Model of an animal in the db, with id as the primary key
"""


class Animal(db.Model):
    __tablename__ = "animal"
    name = db.Column(db.String, unique=True, nullable=False)
    habitat = db.Column(db.String)
    status = db.Column(db.String, nullable=False)
    description = db.Column(db.String)
    scientific_name = db.Column(db.String)
    country = db.Column(db.String)
    organization = db.Column(db.String)
    primary_image = db.Column(db.String)
    secondary_image = db.Column(db.String)
    id = db.Column(db.Integer, unique=True, primary_key=True)
    country_id = db.Column(db.Integer, db.ForeignKey("country.id"))
    organization_id = db.Column(db.Integer, db.ForeignKey("organization.id"))
    country_flag = db.Column(db.String)
    organization_logo = db.Column(db.String)


class Animal_Backup(db.Model):
    __tablename__ = "animal_backup"
    id = db.Column(db.Integer, unique=True, primary_key=True)
    country = db.Column(db.String)
