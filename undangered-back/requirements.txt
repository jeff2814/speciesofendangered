flask==1.0.2
python-gitlab==2.5.0
wikipedia==1.4.0
uwsgi==2.0.18
psycopg2-binary==2.8.6
Flask-Cors==1.10.3
Flask-SQLAlchemy==2.4.4
pytest==6.1.1
