import requests
import json


class NationApi:
    def __init__(self):
        self.endpoint = "http://api.worldbank.org/v2/country/"

    def get_nation(self, name):
        r = requests.get(f"{self.endpoint}{name}?format=json")
        return r.json()[1][0]

    def search_nations(self, keywords, filters):
        r = requests.get(f"{self.endpoint}?format=json&per_page=304")
        return r.json()[1]


if __name__ == "__main__":
    api = NationApi()
    print("Getting Info for BRA")
    print(api.get_nation("BRA"))
    print("Getting All Nations")
    print([nat.get("id") for nat in api.search_nations(None, None)])
