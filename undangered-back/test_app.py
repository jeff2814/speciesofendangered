import json
from app import *


def test_api_animal_info():
    with app.app_context():
        nonexistent_animal = "alien"
        info, code = api_animal_info(nonexistent_animal)
        assert info == ""
        assert code == 204

        animal = "Bear, polar"
        info, code = api_animal_info(animal)
        keys = [
            "country",
            "description",
            "name",
            "scientific_name",
            "primary_image",
            "secondary_image",
            "status",
        ]
        data = json.loads(info.response[0])
        assert code == 200
        assert type(data) == dict
        for key in keys:
            assert data.get(key) is not None


def test_api_nation_info():
    with app.app_context():
        nonexistent_nation = "daddy"
        info, code = api_nation_info(nonexistent_nation)
        assert info == ""
        assert code == 204

        nation = "China"  # numba 1
        info, code = api_nation_info(nation)
        keys = ["name", "capital", "latitude", "longitude", "id"]
        data = json.loads(info.response[0])
        assert code == 200
        assert type(data) == dict
        for key in keys:
            assert data.get(key) is not None


def test_api_organization_info():
    with app.app_context():
        nonexistent_organization = "Monkeys with bananas"
        info, code = api_organization_info(nonexistent_organization)
        assert info == ""
        assert code == 204

        organization = "APE Foundation"
        info, code = api_organization_info(organization)
        keys = ["name", "website", "description"]
        data = json.loads(info.response[0])
        assert code == 200
        assert type(data) == dict
        for key in keys:
            assert data.get(key) is not None


def test_api_animal_search():
    with app.app_context():
        info, code = api_animal_search()
        assert code == 200
        data = json.loads(info.response[0])
        assert type(data) == list


def test_api_nation_search():
    with app.app_context():
        info, code = api_nation_search()
        assert code == 200
        data = json.loads(info.response[0])
        assert type(data) == list


def test_api_organization_search():
    with app.app_context():
        info, code = api_organization_search()
        assert code == 200
        data = json.loads(info.response[0])
        assert type(data) == list


def test_api_animal_search_by_country():
    with app.app_context():
        nonexistent_nation = "daddy"
        info, code = api_animal_search_by_country(nonexistent_nation)
        data = json.loads(info.response[0])
        assert code == 200
        assert type(data) == list
        assert len(data) == 0

        nation = "China"  # numba 1
        info, code = api_animal_search_by_country(nation)
        data = json.loads(info.response[0])
        assert code == 200
        assert type(data) == list


def test_api_animal_search_by_organization():
    with app.app_context():
        nonexistent_organization = "Monkeys with bananas"
        info, code = api_animal_search_by_org(nonexistent_organization)
        data = json.loads(info.response[0])
        assert code == 200
        assert type(data) == list
        assert len(data) == 0

        organization = "APE Foundation"
        info, code = api_animal_search_by_org(organization)
        data = json.loads(info.response[0])
        assert code == 200
        assert type(data) == list


def test_api_organzation_search_by_country():
    with app.app_context():
        nonexistent_nation = "daddy"
        info, code = api_org_search_by_country(nonexistent_nation)
        data = json.loads(info.response[0])
        assert code == 200
        assert type(data) == list
        assert len(data) == 0

        nation = "China"  # numba 1
        info, code = api_org_search_by_country(nation)
        data = json.loads(info.response[0])
        assert code == 200
        assert type(data) == list
