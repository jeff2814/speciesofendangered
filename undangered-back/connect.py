#!/usr/bin/python3

import gitlab
import json
import requests
import time
from threading import Thread

from collections import Counter

PROJECT_NAME = "speciesofendangered"
TOKEN_FILE = "token.json"
GITLAB_URL = "https://gitlab.com/"
CACHE_FILE = "gitlab.json"
INTERVAL = 60

"""
Load token from file
"""


def load_token(token_filename: str) -> str:
    with open(token_filename, "r") as token_file:
        js = json.load(token_file)
        token = js["token"]
        return token


"""
Load the gitlab instance through api
"""


def load_project(token: str):
    gl = gitlab.Gitlab(GITLAB_URL, private_token=token)
    gl.auth()

    projects = gl.projects.list(search=PROJECT_NAME)

    assert projects[0]

    project = projects[0]

    return project


token = load_token(TOKEN_FILE)
project = load_project(token)

"""
Update cached gitlab data on disk
"""


def api_access():
    while True:
        commits = project.commits.list(all=True)
        commit_count = len(commits)
        commit_data = Counter([x.author_name for x in commits])

        issues = project.issues.list(all=True)
        issue_count = len(issues)
        issue_states = Counter([x.state for x in issues])

        data = {
            "commit_count": commit_count,
            "commit_data": dict(commit_data),
            "issue_count": issue_count,
            "issue_states": dict(issue_states),
        }

        with open(CACHE_FILE, "w") as f:
            json.dump(data, f)

        time.sleep(INTERVAL)


# set up separate thread to do the updating
thread = Thread(target=api_access)
thread.daemon = True
thread.start()

"""
Get total number of commits
"""


def get_num_commits():
    with open(CACHE_FILE, "r") as f:
        d = json.load(f)
        return d["commit_count"]


"""
Get detailed commit data
"""


def get_individual_commits():
    with open(CACHE_FILE, "r") as f:
        d = json.load(f)
        return d["commit_data"]


"""
Get total number of issues
"""


def get_num_issues():
    with open(CACHE_FILE, "r") as f:
        d = json.load(f)
        return d["issue_count"]


"""
Get detailed issue data
"""


def get_issue_states():
    with open(CACHE_FILE, "r") as f:
        d = json.load(f)
        return d["issue_states"]


if __name__ == "__main__":
    num_commits = get_num_commits()

    print(f"{num_commits} commit(s)")
