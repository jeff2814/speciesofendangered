from sqlalchemy import func
from model_animal import Animal, Animal_Backup
from model_organization import Organization
from model_country import Country

"""
Collection of helper methods to retrieve animal data
"""


class AnimalQuery:
    def __init__(self):
        pass

    """
    Return animal, searched by name
    """

    def get_animal(self, name):
        result = Animal.query.filter_by(name=name).first()
        serializable = result.__dict__
        serializable.pop("_sa_instance_state")
        return serializable

    """
    Return all animals, sorted by name
    """

    def get_all(self):
        return self.search_animals()

    """
    Return animals with optional filters, sorted by name
    """

    def search_animals(self, keywords=None, filters=None, page=0, entries_per_page=50):
        result = Animal.query.order_by(Animal.name).all()
        serializable = []
        for animal in result:
            s = animal.__dict__
            s.pop("_sa_instance_state")
            serializable.append(s)
        return serializable

    """
    Return animal, searched by country
    """

    def search_animals_by_country(self, country):
        result = Animal.query.filter_by(country=country).all()
        serializable = []
        for animal in result:
            s = animal.__dict__
            s.pop("_sa_instance_state")
            serializable.append(s)
        return serializable

    """
    Return animal, searched by organization
    """

    def search_animals_by_org(self, org):
        result = Animal.query.filter_by(organization=org).all()
        serializable = []
        for animal in result:
            s = animal.__dict__
            s.pop("_sa_instance_state")
            serializable.append(s)
        return serializable

    """
    Return animal, searched by id
    """

    def search_animals_by_id(self, id):
        result = Animal.query.filter_by(id=id).first()
        data = result.__dict__
        data.pop("_sa_instance_state")

        return data

    """
    Get list of countries with animals
    """

    def get_country_count_by_animals(self):
        result = (
            Animal_Backup.query.with_entities(
                Animal_Backup.country, func.count(Animal_Backup.country)
            )
            .group_by(Animal_Backup.country)
            .all()
        )
        return {x: y for x, y in result}


"""
Collection of helper methods to retrieve nation data
"""


class NationQuery:
    def __init__(self):
        pass

    """
    Return nation, searched by name
    """

    def get_nation(self, name):
        result = Country.query.filter_by(name=name).first()
        serializable = result.__dict__
        serializable.pop("_sa_instance_state")
        return serializable

    """
    Return all nations, sorted by name
    """

    def get_all(self):
        return self.search_nations()

    """
    Return nations with optional filters, searched by name
    """

    def search_nations(self, keywords=None, filters=None, page=0, entries_per_page=50):
        result = Country.query.order_by(Country.name).all()
        serializable = []
        for country in result:
            s = country.__dict__
            s.pop("_sa_instance_state")
            serializable.append(s)
        return serializable

    """
    Return nation, searched by id
    """

    def search_nations_by_id(self, id):
        result = Country.query.filter_by(id=id).first()
        data = result.__dict__
        data.pop("_sa_instance_state")

        return data


"""
Collection of helper methods to retrieve organization data
"""


class OrganizationQuery:
    def __init__(self):
        pass

    """
    Return organization, searched by name
    """

    def get_organization(self, name):
        result = Organization.query.filter_by(name=name).first()
        serializable = {k: str(v) for k, v in result.__dict__.items()}
        serializable.pop("_sa_instance_state")
        return serializable

    """
    Return all organizations, sorted by name
    """

    def get_all(self):
        return self.search_organizations()

    """
    Return organizations with optional filters, sorted by name
    """

    def search_organizations(
        self, keywords=None, filters=None, page=0, entries_per_page=50
    ):
        result = Organization.query.order_by(Organization.name).all()
        serializable = []
        for org in result:
            s = {k: str(v) for k, v in org.__dict__.items()}
            s.pop("_sa_instance_state")
            serializable.append(s)
        return serializable

    """
    Return organization, searched by country
    """

    def search_orgs_by_country(self, name):
        result = Organization.query.filter_by(country=name).all()
        serializable = []
        for org in result:
            s = {k: str(v) for k, v in org.__dict__.items()}
            s.pop("_sa_instance_state")
            serializable.append(s)
        return serializable

    """
    Return organization, searched by id
    """

    def search_orgs_by_id(self, id):
        result = Organization.query.filter_by(id=id).first()

        data = result.__dict__
        data.pop("_sa_instance_state")

        return data


if __name__ == "__main__":
    api = OrganizationQuery()
    print(api.search_organizations(None, None))
