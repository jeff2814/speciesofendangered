import requests
import wikipedia
import json
from typing import List

CATEGORIES = [
    "Category:Wildlife_conservation_organizations",
    "Category:Animal_conservation_organizations",
    "Category:Conservation_and_environmental_foundations",
    "Category:Animal_welfare_organizations",
]


class OrganizationApi:
    def __init__(self):
        self.endpoint = "https://en.wikipedia.org/w/api.php?action=query&generator=categorymembers&prop=categories&cllimit=max&gcmlimit=max&format=json&gcmtitle="
        self.organizations = self._get_all_organizations()

    def _load_category(self, category: str):
        url = self.endpoint + category
        data = requests.get(url).text
        return json.loads(data)

    def _load_page_names_from_category(self, category: str):
        data = self._load_category(category)["query"]["pages"]
        return [
            v["title"] for k, v in data.items() if not v["title"].startswith("Category")
        ]

    def _get_all_organizations(self) -> List[str]:
        combined = [
            item
            for category in CATEGORIES
            for item in self._load_page_names_from_category(category)
        ]
        return combined

    def get_organization(self, name):
        try:
            pg = wikipedia.page(name)
            return pg.summary
        except Exception as e:
            return "Description Not Found"

    def search_organizations(self, keywords, filters):
        return self.organizations


if __name__ == "__main__":
    orgs_api = OrganizationApi()
    orgs = orgs_api.search_organizations(None, None)
    print(f"Orgs: {orgs}")
    print(f"Summary for {orgs[0]}: {orgs_api.get_organization(orgs[0])}")
