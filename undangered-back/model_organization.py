from models import db

"""
Model of an organization in the db, with id as the primary key
"""


class Organization(db.Model):
    __tablename__ = "organization"
    name = db.Column(db.String, unique=True, nullable=False)
    country = db.Column(db.String)
    description = db.Column(db.String)
    primary_image = db.Column(db.String)
    secondary_image = db.Column(db.String)
    year = db.Column(db.Numeric)
    website = db.Column(db.String)
    id = db.Column(db.Integer, unique=True, nullable=False, primary_key=True)
    country_flag = db.Column(db.String)
