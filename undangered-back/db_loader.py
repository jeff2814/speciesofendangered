import json
import time
import requests
import wikipedia
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
from organization import OrganizationApi
from nation import NationApi
from sqlalchemy import Table, MetaData, create_engine
from db import Animal, Country, Organization, db


class DBLoader:
    def __init__(self):
        settings_file = "db.json"
        with open(settings_file, "r") as f:
            settings = json.load(f)

        DB_URL = f'postgresql://{settings["username"]}:{settings["password"]}@{settings["endpoint"]}:{settings["port"]}/{settings["db_name"]}'
        self.engine = create_engine(DB_URL)

    def get_image(self, name, num=1):
        try:
            link = f"""https://www.google.com/search?q={name}&sxsrf=ALeKk012bgiFXfaXhcOjfe-VztER0WGyVQ:1602877895234&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjT78C88bnsAhWJiOAKHdJVDH4Q_AUoAXoECA8QAw&cshid=1602877977073568&biw=1440&bih=700"""
            html = requests.get(link).content
            soup = BeautifulSoup(html, "html.parser")
            print(soup.findAll("img")[num])
            return soup.findAll("img")[num]["src"]
        except:
            return ""

    def get_animal_description(self, name):
        try:
            page = wikipedia.search(name)[0]
            return wikipedia.summary(page)
        except Exception as e:
            return ""

    def get_animal_country(self, scientific_name):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        driver = webdriver.Chrome(options=chrome_options)
        try:
            driver.get("https://www.iucnredlist.org/search")
            search = driver.find_element_by_xpath(
                '//*[@id="nav-search"]/div/form/input'
            )
            search.send_keys(scientific_name)
            time.sleep(2)
            top_link = driver.find_element_by_xpath(
                '//*[@id="nav-search"]/div/div/section[1]/ol/li[1]/a'
            )
            top_link.click()
            time.sleep(2)
            try:
                country = driver.find_element_by_xpath(
                    '//*[@id="geographic-range"]/div[1]/div/div/div[2]/p'
                )
            except:
                country = driver.find_element_by_xpath(
                    '//*[@id="geographic-range"]/div[1]/div[1]/div/div[2]/p'
                )
            top_country = country.text.split(";")[0]
            splice = (
                len(top_country) if top_country.find("(") < 0 else top_country.find("(")
            )
            return top_country[:splice].strip(" ")
        except Exception as e:
            return ""
        finally:
            driver.close()

    def load_animals(self):
        html = requests.get(
            "https://awionline.org/content/list-endangered-species"
        ).content
        soup = BeautifulSoup(html, "html.parser")
        rows = soup.find_all("tr")
        with self.engine.connect() as conn:
            conn.execute("DELETE FROM animal")
            for row in rows:
                data = [td.get_text() for td in row.find_all("td")]
                if len(data) == 3:
                    name, scientific_name, status = data[0], data[1], data[2]
                    country = self.get_animal_country(scientific_name)
                    image = self.get_image(name)
                    desc = self.get_animal_description(name)
                    try:
                        name, country, desc = (
                            name.replace("'", ""),
                            country.replace("'", ""),
                            desc.replace("'", ""),
                        )
                        stmt = f"""INSERT INTO animal(name, status, description, scientific_name, country, image)
                               VALUES('{name}', '{status}', '{desc}', '{scientific_name}', '{country}', '{image}')"""
                        conn.execute(stmt)
                        print(f"added {name} to db")
                    except Exception as e:
                        continue

    def load_orgs(self):
        api = OrganizationApi()
        org_names = api.search_organizations(None, None)
        org_info = {name: api.get_organization(name) for name in org_names}
        with self.engine.connect() as conn:
            conn.execute("DELETE FROM organization")
            for name, description in org_info.items():
                try:
                    name, description = name.replace("'", ""), description.replace(
                        "'", ""
                    )
                    stmt = f"""INSERT INTO organization(name, description)
                           VALUES('{name}', '{description}')"""
                    print(stmt)
                    conn.execute(stmt)
                except TypeError as e:
                    continue

    def load_countries(self):
        api = NationApi()
        country_names = api.search_nations(None, None)
        country_info = [api.get_nation(country.get("id")) for country in country_names]
        with self.engine.connect() as conn:
            conn.execute("DELETE FROM country")
            for country in country_info:
                name = country["name"].replace("'", "")
                capital = country["capitalCity"].replace("'", "")
                region = country["region"]["value"].replace("'", "")
                if capital:
                    try:
                        stmt = f"""INSERT INTO country(id, name, capital, region, latitude, longitude)
                           VALUES('{country['iso2Code']}', '{name}', '{capital}', '{region}', '{country['latitude']}', '{country['longitude']}')"""
                        print(stmt)
                        conn.execute(stmt)
                    except TypeError as e:
                        continue

    def update_images(self):
        countries = Country.query.all()
        for country in countries:
            country.primary_image = self.get_image(f"{country.name} flag")
            country.secondary_image = self.get_image(f"{country.name} map")
            db.session.commit()

        orgs = Organization.query.all()
        for org in orgs:
            org.primary_image = self.get_image(org.name, 1)
            org.secondary_image = self.get_image(org.name, 2)
        db.session.commit()

        animals = Animal.query.all()
        for animal in animals:
            if not animal.secondary_image:
                animal.primary_image = self.get_image(animal.name, 1)
                animal.secondary_image = self.get_image(animal.name, 2)
                db.session.commit()

    def get_first_search_result(self, name):
        try:
            link = f"""https://www.google.com/search?q={name}"""
            html = requests.get(link).content
            soup = BeautifulSoup(html, "html.parser")
            result_div = soup.find_all("div", attrs={"class": "kCrYT"})
            for r in result_div:
                try:
                    link = r.find("a", href=True)["href"]
                    return link[7 : link.find("&")]
                except:
                    continue
            return ""
        except:
            return ""


if __name__ == "__main__":
    loader = DBLoader()
    #    loader.update_images()

    orgs = Organization.query.all()
    for org in orgs:
        website = loader.get_first_search_result(org.name)
        org.website = website
        db.session.commit()
