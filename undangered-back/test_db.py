from app import app
from db import *

animal_query = AnimalQuery()
nation_query = NationQuery()
organization_query = OrganizationQuery()


def test_api_animal_info():
    with app.app_context():
        nonexistent_animal = "alien"
        try:
            info = animal_query.get_animal(nonexistent_animal)
            assert False
        except:
            pass

        animal = "Bear, polar"
        data = animal_query.get_animal(animal)
        keys = [
            "country",
            "description",
            "name",
            "scientific_name",
            "primary_image",
            "secondary_image",
            "status",
        ]
        assert type(data) == dict
        for key in keys:
            assert data.get(key) is not None


def test_api_nation_info():
    with app.app_context():
        nonexistent_nation = "daddy"
        try:
            info = nation_query.get_nation(nonexistent_nation)
            assert False
        except:
            pass

        nation = "China"  # numba 1
        data = nation_query.get_nation(nation)
        keys = ["name", "capital", "latitude", "longitude", "id"]
        assert type(data) == dict
        for key in keys:
            assert data.get(key) is not None


def test_api_organization_info():
    with app.app_context():
        nonexistent_organization = "Monkeys with bananas"
        try:
            info = organization_query.get_organization(nonexistent_organization)
            assert False
        except:
            pass

        organization = "APE Foundation"
        data = organization_query.get_organization(organization)
        keys = ["name", "website", "description"]
        assert type(data) == dict
        for key in keys:
            assert data.get(key) is not None


def test_api_animal_search():
    with app.app_context():
        data = animal_query.search_animals()
        assert type(data) == list


def test_api_nation_search():
    with app.app_context():
        data = nation_query.search_nations()
        assert type(data) == list


def test_api_organization_search():
    with app.app_context():
        data = organization_query.search_organizations()
        assert type(data) == list


def test_api_animal_search_by_country():
    with app.app_context():
        nonexistent_nation = "daddy"
        data = animal_query.search_animals_by_country(nonexistent_nation)
        assert type(data) == list
        assert len(data) == 0

        nation = "China"  # numba 1
        data = animal_query.search_animals_by_country(nation)
        assert type(data) == list


def test_api_animal_search_by_organization():
    with app.app_context():
        nonexistent_organization = "Monkeys with bananas"
        data = animal_query.search_animals_by_org(nonexistent_organization)
        assert type(data) == list
        assert len(data) == 0

        organization = "APE Foundation"
        data = animal_query.search_animals_by_org(organization)
        assert type(data) == list


def test_api_organzation_search_by_country():
    with app.app_context():
        nonexistent_nation = "daddy"
        data = organization_query.search_orgs_by_country(nonexistent_nation)
        assert type(data) == list
        assert len(data) == 0

        nation = "China"  # numba 1
        data = organization_query.search_orgs_by_country(nation)
        assert type(data) == list
