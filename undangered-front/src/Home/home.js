import React from "react";
import { Carousel, Container, Row, Col } from "react-bootstrap";

import MainNav from "../MainNav/mainNav.js";
import "../bootstrap.css";
import "./home.css";
import HomeIcon from "./homeIcon.js";

const imgData = [
  [
    "Koala",
    "https://cdn.britannica.com/26/162626-050-3534626F/Koala.jpg",
    "Koala",
  ],
  [
    "Bactrian Camel",
    "https://upload.wikimedia.org/wikipedia/commons/8/82/2011_Trampeltier_1528.JPG",
    "Camel,%20Bactrian",
  ],
  [
    "Three-toed Sloth",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Bicho-pregui%C3%A7a_3.jpg/1920px-Bicho-pregui%C3%A7a_3.jpg",
    "Sloth,%20Brazilian%20three-toed",
  ],
  [
    "Indian Softshell Turtle",
    "https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/Indian_softshell_turtle_%28Nilssonia_gangetica%29_Babai_River.jpg/1920px-Indian_softshell_turtle_%28Nilssonia_gangetica%29_Babai_River.jpg",
    "Turtle,%20Indian%20softshell",
  ],
  [
    "Clouded Leopard",
    "https://upload.wikimedia.org/wikipedia/commons/2/2d/CloudedLeopard.jpg",
    "Leopard,%20clouded",
  ],
];

/*
Generates the slideshow displaying 5 spotlight animals
Returns a slideshow displaying 5 spotlight animals
*/
function HomeCarousel() {
  let carouselItems = imgData.map((data) => (
    <Carousel.Item key={data[0]}>
      <a href={"https://www.undangered.ml/animals/" + data[2]}>
        <img className="home-carousel" src={data[1]} alt={data[0]} />
      </a>
      <Carousel.Caption>
        <h3 className="caption"> {data[0]} </h3>
      </Carousel.Caption>
    </Carousel.Item>
  ));
  return (
    <React.Fragment>
      <Carousel>{carouselItems}</Carousel>
    </React.Fragment>
  );
}

function HomeDisplay() {
  return (
    <React.Fragment>
      <MainNav />
      <h1 className="title">Undangered</h1>
      <HomeCarousel />
      <Container style={{ marginTop: "10%", marginBottom: "25%" }}>
        <Row>
          <Col>
            <HomeIcon
              icon="https://www.flaticon.com/svg/static/icons/svg/141/141796.svg"
              title="Animals"
              text="Click here to learn more about animals!"
              link="/animals"
            />
          </Col>
          <Col>
            <HomeIcon
              icon="https://image.flaticon.com/icons/png/512/1083/1083497.png"
              title="Organizations"
              text="Click here to learn more about organizations!"
              link="/organizations"
            />
          </Col>
          <Col>
            <HomeIcon
              icon="https://icons.iconarchive.com/icons/dtafalonso/modern-xp/512/ModernXP-73-Globe-icon.png"
              title="Countries"
              text="Click here to learn more about countries!"
              link="/countries"
            />
          </Col>
        </Row>
      </Container>
    </React.Fragment>
  );
}
class Home extends React.Component {
  render() {
    return <HomeDisplay />;
  }
}

export default Home;
