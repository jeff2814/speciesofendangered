import React, { useState } from "react";
import { Card } from "react-bootstrap";
import { useSpring, a } from "react-spring";
import "../bootstrap.css";

function HomeIcon(props) {
  const [hover, setHover] = useState(false);
  const spring = useSpring({
    opacity: hover ? 0.15 : 1,
  });
  const textStyle = useSpring({
    opacity: hover ? 1 : 0,
  });
  function route() {
    window.location.assign(props.link);
  }

  return (
    <Card
      style={{ margin: "2%", maxWidth: "100%", background: "white" }}
      onClick={() => {
        route();
      }}
      onMouseEnter={(event) => setHover(true)}
      onMouseLeave={(event) => setHover(false)}
    >
      <a.div style={spring}>
        <Card.Img
          variant="top"
          src={props.icon}
          style={{ height: "auto", padding: "10px", alignContent: "center" }}
        />
      </a.div>
      <a.div style={textStyle}>
        <Card.Text
          style={{
            textAlign: "center",
            color: "black",
            position: "absolute",
            width: "100%",
            top: "40%",
          }}
        >
          {props.text}
        </Card.Text>
      </a.div>
      <Card.Body>
        <Card.Title
          style={{
            alignContent: "center",
            textAlign: "center",
            color: "black",
          }}
        >
          {props.title}
        </Card.Title>
      </Card.Body>
    </Card>
  );
}

export default HomeIcon;
