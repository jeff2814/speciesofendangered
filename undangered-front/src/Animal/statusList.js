const statusList = [
  { name: "Select All", value: "Select All" },
  { name: "Threatened", value: "Threatened" },
  { name: "Endangered", value: "Endangered" },
  { name: "XN", value: "XN" },
];

export default statusList;
