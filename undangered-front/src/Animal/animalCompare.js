import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Col, Row, Container } from "react-bootstrap";

import MainNav from "../MainNav/mainNav.js";
import RenderAnimal from "./renderAnimal.js";
import getData from "../util.js";
import "../bootstrap.css";
import "./animal.css";

/* 
Get the animal data and render its table
*/
function AnimalData() {
  let def = [
    {
      name: "name",
      habitat: "habitat",
      status: "status",
      description: "description",
      scientific_name: "scientific_name",
      country: "country",
      organization: "organization",
      primary_image: "",
    },
  ];

  // Get animal data
  let { animalsRaw } = useParams();
  let animals = animalsRaw.split("bigboi");
  let name1 = animals[0];
  let name2 = animals[1];
  const [animal1, setAnimal1] = useState(def);
  const [animal2, setAnimal2] = useState(def);
  useEffect(() => {
    getData(setAnimal1, "https://api.undangered.ml/api/animalInfo/" + name1);
  }, [name1]);
  useEffect(() => {
    getData(setAnimal2, "https://api.undangered.ml/api/animalInfo/" + name2);
  }, [name2]);

  return (
    <React.Fragment>
      <h1 style={{ textAlign: "center" }}>
        {" "}
        Comparing {animal1.name} with {animal2.name}{" "}
      </h1>
      <Container style={{ maxWidth: "70%" }}>
        <Row>
          <Col>{RenderAnimal(animal1)}</Col>
          <Col>{RenderAnimal(animal2)}</Col>
        </Row>
      </Container>
    </React.Fragment>
  );
}

class AnimalCompare extends React.Component {
  render() {
    return (
      <div>
        <MainNav />
        <AnimalData />
      </div>
    );
  }
}

export default AnimalCompare;
