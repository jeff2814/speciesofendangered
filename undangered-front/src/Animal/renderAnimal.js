import React from "react";
import { Card } from "react-bootstrap";
import LoadAction from "react-loading";
import "../bootstrap.css";
import "./animal.css";

const style = {
  spinner: {
    fill: "white",
    width: "10%",
    height: "10%",
  },
};

/*
Return the data for the animal instance
*/
function RenderAnimal(animalJSON) {
  // Loading screen
  if (animalJSON.name === "name")
    return (
      <LoadAction
        className="load-spinner"
        type="spinningBubbles"
        style={style.spinner}
        color="white"
      />
    );

  // Create linking element for organization
  let organizationLink = "";
  if (animalJSON.organization)
    organizationLink = (
      <React.Fragment>
        Organization:{" "}
        <a href={"/organizations/" + encodeURI(animalJSON.organization)}>
          {animalJSON.organization}
          <img
            style={{ maxHeight: "30px", width: "auto", marginLeft: "10px" }}
            src={animalJSON.organization_logo}
            alt="flag"
          ></img>
        </a>
      </React.Fragment>
    );

  // Create linking element for country
  let countryLink = "";
  if (animalJSON.country)
    countryLink = (
      <React.Fragment>
        Country:{" "}
        <a href={"/countries/" + encodeURI(animalJSON.country)}>
          {animalJSON.country}
        </a>
        <img
          style={{ maxHeight: "20px", width: "auto", marginLeft: "10px" }}
          src={animalJSON.country_flag}
          alt="flag"
        ></img>
      </React.Fragment>
    );

  return (
    <React.Fragment>
      <h2 style={{ textAlign: "center" }}> {animalJSON.name}</h2>
      <Card className="animal-card" variant="outlined">
        <Card.Header>
          <div className="animal-header">
            <img
              className="center-img"
              alt={"Photo of " + animalJSON.name}
              src={animalJSON.primary_image}
            />
            <img
              className="center-img"
              alt={"Photo of " + animalJSON.name}
              src={animalJSON.secondary_image}
            />
          </div>
          <div className="animal-header">
            <Card.Text>Scientific Name: {animalJSON.scientific_name}</Card.Text>
            <Card.Text>Status: {animalJSON.status}</Card.Text>
            {countryLink}
            <br/>
            {organizationLink}
          </div>
        </Card.Header>
      </Card>
      <Card
        style={{
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "1%",
          maxWidth: "75%",
        }}
      >
        <Card.Body>
          <Card.Text>{animalJSON.description}</Card.Text>
        </Card.Body>
      </Card>
    </React.Fragment>
  );
}

export default RenderAnimal;
