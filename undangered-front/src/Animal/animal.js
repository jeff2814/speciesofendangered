import React, { useEffect, useState } from "react";
import axios from "axios";
import DataTable from "react-data-table-component";
import LoadAction from "react-loading";
import SelectSearch from "react-select-search";
import { Button } from "react-bootstrap";

import MainNav from "../MainNav/mainNav.js";
import Searchbar from "../Searchbar/searchbar.js";
import { filterData } from "../util.js";
import tableCols from "./tableCols.js";
import statusList from "./statusList.js";

import "../bootstrap.css";
import "./animal.css";
import "../selectSearch.css";

const style = {
  spinner: {
    fill: "white",
    width: "10%",
    height: "10%",
  },
  inline: {
    display: "inline-block",
    position: "relative",
    float: "right",
    marginRight: "20px",
  },
};

/* 
Makes an axios request to update the data for the table
*/
function fillData(setLoading, setData) {
  setLoading(true);
  axios
    .get("https://api.undangered.ml/api/animalSearch")
    .then((res) => {
      setData(res.data);
      setLoading(false);
    })
    .catch((err) => {
      setData([]);
      setLoading(false);
    });
}

/* 
Makes an axios request to get the list of countries
*/
function fillCountries(setLoading, setCountries) {
  setLoading(true);

  axios
    .get("https://api.undangered.ml/api/nationSearch")
    .then((res) => {
      let temp = [...new Set(res.data.map((instance) => instance.name))];
      temp.sort();
      temp.unshift("Select All");
      setCountries(
        temp.map((instance) => ({ name: instance, value: instance }))
      );
      setLoading(false);
    })
    .catch((err) => {
      setCountries([]);
      setLoading(false);
    });
}

/* 
Makes an axios request to get the list of organizations
*/
function fillOrgs(setLoading, setOrgs) {
  setLoading(true);

  axios
    .get("https://api.undangered.ml/api/organizationSearch")
    .then((res) => {
      let temp = [...new Set(res.data.map((instance) => instance.name))];
      temp.sort();
      temp.unshift("Select All");
      setOrgs(temp.map((instance) => ({ name: instance, value: instance })));
      setLoading(false);
    })
    .catch((err) => {
      setOrgs([]);
      setLoading(false);
    });
}

/*
Generates the table displaying the animal table
Returns:
1) Filters
2a) a table displaying the animal table if data has finished loading
2b) a spinner if the data has not finished loading
*/
function AnimalTable(props) {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [curData, setCurData] = useState([]);
  const [filter1, setFilter1] = useState("");
  const [filter2, setFilter2] = useState("");
  const [filter3, setFilter3] = useState("");
  const [countries, setCountries] = useState([]);
  const [orgs, setOrgs] = useState([]);
  const [selected, setSelected] = useState([]);
  const [search, setSearch] = useState("");

  useEffect(() => {
    fillData(setLoading, setData);
    fillCountries(setLoading, setCountries);
    fillOrgs(setLoading, setOrgs);
  }, []);

  useEffect(() => {
    setCurData(
      filterData(
        data,
        ["status", "country", "organization"],
        [filter1, filter2, filter3]
      )
    );
  }, [data, filter1, filter2, filter3]);

  let searchData = curData;

  // Filter out the search terms
  if (search !== "") {
    searchData = searchData.filter((model) => {
      for (var key in model) {
        if (
          key === "description" ||
          key === "primary_image" ||
          key === "secondary_image"
        )
          continue;
        if (
          JSON.stringify(model[key])
            .toLowerCase()
            .indexOf(search.toLowerCase()) >= 0
        )
          return true;
      }
      return false;
    });
  }

  function handleChange(state) {
    setSelected(state.selectedRows);
  }

  function compareSelected() {
    if (selected.length !== 2) {
      alert("Please Select exactly 2 instances to compare.");
    } else {
      let animals = selected.reduce((a, b) => a.name + "bigboi" + b.name);
      window.location.href = "/animals/compare/" + animals;
    }
  }

  return !loading ? (
    <React.Fragment>
      <div className="animal-filter-dropdowns" id="animals-filters">
        <h5 className="animal-filter-header">Filters:</h5>
        <SelectSearch
          options={statusList}
          onChange={setFilter1}
          search
          placeholder="Select a conservation status"
        />
        <SelectSearch
          options={countries}
          onChange={setFilter2}
          search
          placeholder="Select a country"
        />
        <SelectSearch
          options={orgs}
          onChange={setFilter3}
          search
          placeholder="Select an organization"
        />
        <Searchbar onSearch={(value) => setSearch(value)} />
        <Button
          style={style.inline}
          variant="secondary"
          onClick={compareSelected}
        >
          Compare Selected
        </Button>
      </div>
      <div className="table-div" id="animals-table-div">
        <DataTable
          columns={tableCols(search)}
          data={searchData}
          pagination
          compact
          selectableRows
          onSelectedRowsChange={handleChange}
          striped
          highlightOnHover
          pointerOnHover
          persistTableHead
        />
      </div>
    </React.Fragment>
  ) : (
    <div className="table-div" id="animals-table-div">
      <LoadAction
        className="load-spinner"
        type="spinningBubbles"
        style={style.spinner}
        color="white"
      />
    </div>
  );
}

class Animal extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };
  }

  render() {
    return (
      <React.Fragment>
        <MainNav />
        <h1 className="title" id="animals-title">
          Animals
        </h1>
        <div
          style={{
            textAlign: "right",
            margin: "0",
            paddingRight: "5%",
          }}
        ></div>
        <AnimalTable search={this.state.value} />
      </React.Fragment>
    );
  }
}

export default Animal;
