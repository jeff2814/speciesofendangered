import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import MainNav from "../MainNav/mainNav.js";
import RenderAnimal from "./renderAnimal.js";
import getData from "../util.js";
import "../bootstrap.css";
import "./animal.css";

/* 
Get the animal data and render its table
*/
function AnimalData() {
  let def = {
    name: "name",
    habitat: "habitat",
    status: "status",
    description: "description",
    scientific_name: "scientific_name",
    country: "country",
    organization: "organization",
    primary_image: "",
  };

  // Get animal data
  let { animal } = useParams();
  const [animalJSON, setAnimalJSON] = useState(def);
  useEffect(() => {
    let animalName = animal;
    getData(
      setAnimalJSON,
      "https://api.undangered.ml/api/animalInfo/" + animalName
    );
  }, [animal]);

  return RenderAnimal(animalJSON);
}

class AnimalInstance extends React.Component {
  render() {
    return (
      <div>
        <MainNav />
        <AnimalData />
      </div>
    );
  }
}

export default AnimalInstance;
