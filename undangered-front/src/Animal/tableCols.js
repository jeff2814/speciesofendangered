import React from "react";
import Highlight from "react-highlighter";

/*
Returns the columns for the Animal model table
*/
function tableCols(searchTerm) {
  const cols = [
    {
      name: "Animal",
      selector: "name",
      sortable: true,
      cell: (row) => (
        <a href={"/animals/" + encodeURI(row.name)}>
          <Highlight search={searchTerm}>{row.name}</Highlight>
        </a>
      ),
    },
    {
      name: "Scientific Name",
      selector: "scientific_name",
      sortable: true,
      cell: (row) => (
        <Highlight search={searchTerm}>{row.scientific_name}</Highlight>
      ),
    },
    {
      name: "Conservation Status",
      selector: "status",
      sortable: true,
      cell: (row) => <Highlight search={searchTerm}>{row.status}</Highlight>,
    },
    {
      name: "Description",
      selector: "description",
      sortable: false,
      omit: true,
      cell: (row) => (
        <Highlight search={searchTerm}>{row.description}</Highlight>
      ),
    },
    {
      name: "Country",
      selector: "country",
      sortable: true,
      cell: (row) => (
        <a href={"/countries/" + encodeURI(row.country)}>
          <Highlight search={searchTerm}>{row.country}</Highlight>
        </a>
      ),
    },
    {
      name: "Supporting Organization",
      selector: "organization",
      sortable: true,
      cell: (row) => (
        <a href={"/organizations/" + encodeURI(row.organization)}>
          <Highlight search={searchTerm}>{row.organization}</Highlight>
        </a>
      ),
    },
  ];
  return cols;
}

export default tableCols;
