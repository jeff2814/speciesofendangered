import React, { useState, useEffect } from "react";
import { Container, CardDeck } from "react-bootstrap";

import MainNav from "../MainNav/mainNav.js";
import getData from "../util.js";
import BioCard from "./bioCard.js";
import IconCard from "./iconCard.js";
import IconDeck from "./iconDeck.js";
import devs from "./devs.js";
import gitlab from "../assets/images/tech/gitlab.png";
import postman from "../assets/images/tech/postman.png";
import "../bootstrap.css";
import "./about.css";

/* 
Returns the # of issues for the GitLab repo
*/
function Issues() {
  const [issues, setIssues] = useState(0);
  useEffect(() => {
    getData(setIssues, "https://api.undangered.ml/api/issueCount");
  }, []);
  return JSON.stringify(issues);
}

/* 
Returns the # of commits for the GitLab repo
*/
function Commits() {
  const [commits, setCommits] = useState(0);
  useEffect(() => {
    getData(setCommits, "https://api.undangered.ml/api/commitCount");
  }, []);
  return JSON.stringify(commits);
}

/*
Returns the # of commits by this dev by using the contributors list
*/
function Bio(dev, contrib) {
  let numCommits = 0;
  var i;
  for (i = 0; i < contrib.length; i++) {
    let element = contrib[i];
    if (dev.usernames.indexOf(element.name) > -1) {
      numCommits += element.commits;
    }
  }

  return (
    <BioCard
      key={dev.name}
      image={dev.img}
      name={dev.name}
      role={`${dev.role}: ${numCommits} commits`}
      bio={dev.bio}
      link={dev.link}
    />
  );
}

/*
Returns a table displaying the devs and their info
*/
function DeveloperTable() {
  const [contrib, setContrib] = useState([
    { name: "Daniel Gao", commits: 0 },
    { name: "Akshay Mantri", commits: 0 },
    { name: "Huy Tran", commits: 0 },
    { name: "Jeffery Wu", commits: 0 },
    { name: "Michael Yang", commits: 0 },
  ]);

  useEffect(() => {
    getData(
      setContrib,
      "https://gitlab.com/api/v4/projects/21273733/repository/contributors"
    );
  }, []);

  let devBios = [];
  for (let i = 0; i < devs.length; i++) {
    devBios[i] = Bio(devs[i], contrib);
  }

  return (
    <Container>
      <CardDeck style={{ justifyContent: "space-evenly" }}>{devBios}</CardDeck>
    </Container>
  );
}

class About extends React.Component {
  render() {
    return (
      <React.Fragment>
        <MainNav />
        <h1 className="title">About Us</h1>
        <DeveloperTable />
        <h4 className="center-text">
          We have {<Commits />} total GitLab commits and {<Issues />} total
          GitLab issues
        </h4>
        <h1 className="title">Our Code and Documentation</h1>
        <Container>
          <CardDeck style={{ justifyContent: "space-evenly" }}>
            <IconCard
              icon={gitlab}
              title={"GitLab Repository"}
              link="https://gitlab.com/jeff2814/speciesofendangered/"
            />
            <IconCard
              icon={postman}
              title={"Postman Documentation"}
              link="https://documenter.getpostman.com/view/6753174/TVRd8B7N"
            />
          </CardDeck>
        </Container>
        <div>
          <Container>
            <IconDeck />
          </Container>
        </div>
      </React.Fragment>
    );
  }
}

export default About;
