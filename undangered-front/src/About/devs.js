const devs = [
  {
    name: "Daniel Gao (Team Lead)",
    usernames: ["Daniel Gao"],
    role: "Backend Engineer",
    img:
      "https://media-exp1.licdn.com/dms/image/C4D03AQHvHijCSEP_ew/profile-" +
      "displayphoto-shrink_800_800/0?e=1606953600&v=beta&t=PuAYSoJ_IqJoY" +
      "b0zaozoPKcJ4_lzi5v4EXrFlIzCKHA",
    bio: "Hi, my name is Daniel, and I am a Junior at UT. I am really short.",
    link: "https://www.linkedin.com/in/daniel-gao21/",
  },
  {
    name: "Akshay Mantri",
    usernames: ["AkshayMantri", "Akshay Mantri"],
    role: "Frontend Developer",
    img:
      "https://media-exp1.licdn.com/dms/image/C4E03AQH9iQxvpqZ6IA/profile-" +
      "displayphoto-shrink_800_800/0?e=1606953600&v=beta&t=WIoRUYgx-" +
      "jeDTIuHtDOZxinBt686xtnedU0JnCtJAlU",
    bio: "Hi, I'm Akshay, and I am a Junior at UT. I love to play sports.",
    link: "https://www.linkedin.com/in/akshay-mantri-987926191/",
  },
  {
    name: "Huy Tran",
    usernames: ["HuyDTran121", "huydtran121"],
    role: "Frontend Developer",
    img:
      "https://media-exp1.licdn.com/dms/image/C4E03AQFDvGJpPjC0Iw/profile-" +
      "displayphoto-shrink_800_800/0?e=1606953600&v=beta&t=jZfCaUH8x" +
      "3CweJOWDyI2Q0Sax5PqKvTSnScRK-3gIAY",
    bio:
      "Hi, I'm Huy, and I am a Junior at UT. I" +
      " have the shortest name in the group.",
    link: "https://www.linkedin.com/in/huy-tran-5036b4174/",
  },
  {
    name: "Jeffery Wu",
    usernames: ["Jeffery Wu", "https://github.com/jeff2814/Blurry-Math.git"],
    role: "DevOps Engineer",
    img:
      "https://media-exp1.licdn.com/dms/image/C5603AQH9hYqvx-xRbg/profile-" +
      "displayphoto-shrink_800_800/0?e=1608768000&v=beta&t=ArWy9Q7B3Pbhqi" +
      "n8Ffgpl-Vkq5UU5UjZIlusnXyzWhM",
    bio:
      "Hi, my name is Jeffery, and I am a Junior at UT. I am an avid K-drama" +
      " lover.",
    link: "https://www.linkedin.com/in/jeff2814/",
  },
  {
    name: "Michael Yang",
    usernames: ["Michael Yang"],
    role: "Backend Engineer",
    img:
      "https://media-exp1.licdn.com/dms/image/C4E03AQED3oEZFSpZeA/profile-" +
      "displayphoto-shrink_800_800/0?e=1606953600&v=beta&t=DYx_HE1hKePcfD" +
      "1W8UBVpVzj-SUterKfMWIkQCZ_MFg",
    bio:
      "Hi, my name is Michael, and I am a Junior at UT. I love eating " +
      ", but I swear that I'm not a glutton.",
    link: "https://www.linkedin.com/in/michael-yang-a53597170/",
  },
];

export default devs;
