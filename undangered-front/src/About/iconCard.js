import React from "react";
import { Card } from "react-bootstrap";

import "../bootstrap.css";
import "./about.css";

function IconCard(props) {
  function route() {
    window.location.assign(props.link);
  }

  return (
    <Card
      style={{
        margin: "2%",
        minWidth: "21%",
        maxWidth: "21%",
        background: "white",
      }}
      onClick={() => {
        route();
      }}
    >
      <Card.Img
        variant="top"
        src={props.icon}
        style={{ height: "auto", alignContent: "center" }}
      />
      <Card.Body>
        <Card.Title
          style={{
            alignContent: "center",
            textAlign: "center",
            color: "black",
            position: "absolute",
            bottom: "0",
          }}
        >
          {props.title}
        </Card.Title>
        <Card.Text>{props.text}</Card.Text>
      </Card.Body>
    </Card>
  );
}

export default IconCard;
