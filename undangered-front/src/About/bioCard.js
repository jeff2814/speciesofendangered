import React from "react";
import { Card } from "react-bootstrap";

import "../bootstrap.css";
import "./about.css";

function BioCard(props) {
  function route() {
    window.location.assign(props.link);
  }

  return (
    <Card
      style={{
        margin: "2%",
        minWidth: "25%",
        maxWidth: "25%",
        background: "white",
      }}
      onClick={() => {
        route();
      }}
    >
      <Card.Img
        variant="top"
        src={props.image}
        style={{ height: "60%", alignContent: "center" }}
      />
      <Card.Body>
        <Card.Title
          style={{
            alignContent: "center",
            textAlign: "center",
            color: "black",
          }}
        >
          {props.name}
        </Card.Title>
        <Card.Text style={{ alignContent: "center", color: "black" }}>
          {props.role}
        </Card.Text>
        <Card.Text style={{ alignContent: "center", color: "black" }}>
          {props.bio}
        </Card.Text>
      </Card.Body>
    </Card>
  );
}

export default BioCard;
