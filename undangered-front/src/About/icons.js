import aws from "../assets/images/tech/aws.png";
import d3 from "../assets/images/tech/d3.jpg";
import discord from "../assets/images/tech/discord.png";
import docker from "../assets/images/tech/docker.jpeg";
import flask from "../assets/images/tech/flask.png";
import freenom from "../assets/images/tech/freenom.jpg";
import mocha from "../assets/images/tech/mocha.png";
import postgresql from "../assets/images/tech/postgresql.png";
import prettier from "../assets/images/tech/prettier.png";
import pytest from "../assets/images/tech/pytest.png";
import react_bootstrap from "../assets/images/tech/react_bootstrap.png";
import react from "../assets/images/tech/react.png";
import selenium from "../assets/images/tech/selenium.png";
import sqlalchemy from "../assets/images/tech/sqlalchemy.png";

const icons = [
  {
    name: "React",
    image: react,
    link: "https://reactjs.org/",
  },
  {
    name: "React Bootstrap",
    image: react_bootstrap,
    link: "https://react-bootstrap.github.io/",
  },
  {
    name: "Flask",
    image: flask,
    link: "https://flask.palletsprojects.com/en/1.1.x/",
  },
  {
    name: "PostgreSQL",
    image: postgresql,
    link: "https://www.postgresql.org/",
  },
  {
    name: "Docker",
    image: docker,
    link: "https://www.docker.com/",
  },
  {
    name: "Amazon Web Services",
    image: aws,
    link: "https://aws.amazon.com/",
  },
  {
    name: "Selenium",
    image: selenium,
    link: "https://www.selenium.dev/",
  },
  {
    name: "Pytest",
    image: pytest,
    link: "https://docs.pytest.org/en/stable/",
  },
  {
    name: "SQLAlchemy",
    image: sqlalchemy,
    link: "https://www.sqlalchemy.org/",
  },
  {
    name: "Mocha",
    image: mocha,
    link: "https://mochajs.org/",
  },
  {
    name: "D3",
    image: d3,
    link: "https://d3js.org/",
  },
  {
    name: "Discord",
    image: discord,
    link: "https://www.discord.com/",
  },
  {
    name: "Freenom",
    image: freenom,
    link: "https://www.freenom.com/en/index.html?lang=en/",
  },
  {
    name: "Prettier",
    image: prettier,
    link: "https://prettier.io/",
  },
];

export default icons;
