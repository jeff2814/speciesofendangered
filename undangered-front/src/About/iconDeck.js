import React from "react";
import { CardDeck } from "react-bootstrap";

import IconCard from "./iconCard";
import "../bootstrap.css";
import "./about.css";
import icons from "./icons.js";

function IconDeck(props) {
  return (
    <React.Fragment>
      <h1 className="title" id="resources-title">
        Resources
      </h1>
      <CardDeck>
        {icons.map((item) => {
          return (
            <IconCard
              key={item.name}
              icon={item.image}
              title={item.name}
              link={item.link}
            />
          );
        })}
      </CardDeck>
    </React.Fragment>
  );
}

export default IconDeck;
