import React, { useEffect, useState } from "react";
import axios from "axios";
import DataTable from "react-data-table-component";
import LoadAction from "react-loading";
import SelectSearch from "react-select-search";
import { Button } from "react-bootstrap";

import MainNav from "../MainNav/mainNav.js";
import Searchbar from "../Searchbar/searchbar.js";
import { filterData } from "../util.js";
import tableCols from "./tableCols.js";

import "../bootstrap.css";
import "./org.css";
import "../selectSearch.css";

const style = {
  spinner: {
    fill: "white",
    width: "10%",
    height: "10%",
  },
};

/* 
Makes an axios request to update the data for the table
*/
function fillData(setLoading, setData) {
  setLoading(true);

  axios
    .get("https://api.undangered.ml/api/organizationSearch/")
    .then((res) => {
      setData(res.data);
      setLoading(false);
    })
    .catch((err) => {
      setData([]);
      setLoading(false);
    });
}

/* 
Gets the list of countries
*/
function fillCountries(data, setLoading, setCountries) {
  setLoading(true);

  let temp = [...new Set(data.map((instance) => instance.country))];
  temp.sort();
  temp.unshift("Select All");
  setCountries(temp.map((instance) => ({ name: instance, value: instance })));

  setLoading(false);
}

/* 
Gets the list of years the orgs were founded
*/
function fillYearFounded(data, setLoading, setYearFounded) {
  setLoading(true);

  let temp = [...new Set(data.map((instance) => instance.year))];
  temp.sort();
  temp.unshift("Select All");
  setYearFounded(temp.map((instance) => ({ name: instance, value: instance })));

  setLoading(false);
}

/*
Generates the table displaying the organization table
Returns:
1) Filters
2a) a table displaying the organization table if data has finished loading
2b) a spinner if the data has not finished loading
*/
function OrgTable(props) {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [curData, setCurData] = useState([]);
  const [filter1, setFilter1] = useState("");
  const [filter2, setFilter2] = useState("");
  const [countries, setCountries] = useState([]);
  const [yearFounded, setYearFounded] = useState([]);
  const [selected, setSelected] = useState([]);
  const [search, setSearch] = useState("");

  useEffect(() => {
    fillData(setLoading, setData);
  }, []);

  useEffect(() => {
    fillCountries(data, setLoading, setCountries);
    fillYearFounded(data, setLoading, setYearFounded);
  }, [data]);

  useEffect(() => {
    setCurData(filterData(data, ["country", "year"], [filter1, filter2]));
  }, [data, filter1, filter2]);

  let searchData = curData;
  // Filter out search terms
  if (search !== "") {
    searchData = searchData.filter((model) => {
      for (var key in model) {
        if (
          key === "description" ||
          key === "primary_image" ||
          key === "secondary_image"
        )
          continue;
        if (
          JSON.stringify(model[key])
            .toLowerCase()
            .indexOf(search.toLowerCase()) >= 0
        )
          return true;
      }
      return false;
    });
  }
  function handleChange(state) {
    setSelected(state.selectedRows);
  }
  function compareSelected() {
    if (selected.length !== 2) {
      alert("Please Select exactly 2 instances to compare.");
    } else {
      let orgs = selected.reduce((a, b) => a.name + "bigboi" + b.name);
      window.location.href = "/organizations/compare/" + orgs;
    }
  }
  let inline = {
    display: "inline-block",
    position: "relative",
    float: "right",
    marginRight: "20px",
  };
  return !loading ? (
    <React.Fragment>
      <div className="org-filter-dropdowns" id="org-filters">
        <h5 className="org-filter-header">Filters:</h5>
        <SelectSearch
          options={countries}
          onChange={setFilter1}
          search
          placeholder="Select a country"
        />
        <SelectSearch
          options={yearFounded}
          onChange={setFilter2}
          search
          placeholder="Select a year"
        />
        <Searchbar onSearch={(value) => setSearch(value)} />
        <Button style={inline} variant="secondary" onClick={compareSelected}>
          Compare Selected
        </Button>{" "}
      </div>
      <div className="table-div" id="orgs-table-div">
        <DataTable
          columns={tableCols(search)}
          data={searchData}
          pagination
          compact
          striped
          selectableRows
          onSelectedRowsChange={handleChange}
          highlightOnHover
          pointerOnHover
          persistTableHead
        />
      </div>
    </React.Fragment>
  ) : (
    <div className="table-div" id="orgs-table-div">
      <LoadAction
        className="load-spinner"
        type="spinningBubbles"
        style={style.spinner}
        color="white"
      />
    </div>
  );
}

class Org extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };
  }

  render() {
    return (
      <React.Fragment>
        <MainNav />
        <h1 className="title" id="orgs-title">
          Organizations
        </h1>
        <div
          style={{
            textAlign: "right",
            margin: "0",
            paddingRight: "5%",
          }}
        ></div>
        <OrgTable search={this.state.value} />
      </React.Fragment>
    );
  }
}

export default Org;
