import React from "react";
import Highlight from "react-highlighter";

/*
Returns the columns for the Country model table
*/
function tableCols(searchTerm) {
  const cols = [
    {
      name: "Name",
      selector: "name",
      sortable: true,
      cell: (row) => (
        <a href={"/organizations/" + encodeURI(row.name)}>
          <Highlight search={searchTerm}>{row.name}</Highlight>
        </a>
      ),
    },
    {
      name: "Logo",
      selector: "primary_image",
      sortable: false,
      cell: (row) => <img alt="Organization Logo" src={row.primary_image} />,
    },
    {
      name: "Country",
      selector: "country",
      sortable: true,
      cell: (row) => (
        <a href={"/countries/" + encodeURI(row.country)}>
          <Highlight search={searchTerm}>{row.country}</Highlight>
        </a>
      ),
    },
    {
      name: "Description",
      selector: "description",
      sortable: false,
      omit: true,
      cell: (row) => (
        <Highlight search={searchTerm}>{row.description}</Highlight>
      ),
    },
    {
      name: "Year Founded",
      selector: "year",
      sortable: true,
      cell: (row) => <Highlight search={searchTerm}>{row.year}</Highlight>,
    },
    {
      name: "Website",
      selector: "website",
      sortable: false,
      cell: (row) => (
        <a href={row.website}>
          {" "}
          <Highlight search={searchTerm}>{row.website}</Highlight>
        </a>
      ),
    },
  ];
  return cols;
}

export default tableCols;
