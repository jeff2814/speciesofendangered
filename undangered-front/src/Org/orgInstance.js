import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import MainNav from "../MainNav/mainNav.js";
import RenderOrg from "./renderOrg.js";
import getData from "../util.js";
import "../bootstrap.css";
import "./org.css";

function OrgData() {
  // Get organization data
  let { organization } = useParams();
  const [org, setOrg] = useState({ name: "name" });
  const [animals, setAnimals] = useState([]);
  useEffect(() => {
    let orgName = organization;
    getData(
      setOrg,
      "https://api.undangered.ml/api/organizationInfo/" + orgName
    );
    getData(
      setAnimals,
      "https://api.undangered.ml/api/animalsByOrganization/" +
        encodeURI(orgName)
    );
  }, [organization]);
  // Render organization instance
  return RenderOrg(org, animals);
}

class OrgInstance extends React.Component {
  render() {
    return (
      <div>
        <MainNav />
        <OrgData />
      </div>
    );
  }
}

export default OrgInstance;
