import React, { useState, useEffect } from "react";
import { Container, Col, Row } from "react-bootstrap";
import { useParams } from "react-router-dom";

import MainNav from "../MainNav/mainNav.js";
import getData from "../util.js";
import RenderOrg from "./renderOrg.js";
import "../bootstrap.css";
import "./org.css";

function OrgData() {
  // Get organization data
  let { organizationsRaw } = useParams();
  let organizations = organizationsRaw.split("bigboi");
  let name1 = organizations[0];
  let name2 = organizations[1];
  const [org1, setOrg1] = useState({});
  const [animals1, setAnimals1] = useState([]);
  const [org2, setOrg2] = useState({});
  const [animals2, setAnimals2] = useState([]);
  useEffect(() => {
    getData(setOrg1, "https://api.undangered.ml/api/organizationInfo/" + name1);
    getData(
      setAnimals1,
      "https://api.undangered.ml/api/animalsByOrganization/" + encodeURI(name1)
    );
  }, [name1]);
  useEffect(() => {
    getData(setOrg2, "https://api.undangered.ml/api/organizationInfo/" + name2);
    getData(
      setAnimals2,
      "https://api.undangered.ml/api/animalsByOrganization/" + encodeURI(name2)
    );
  }, [name2]);
  // Render organization instance
  return (
    <React.Fragment>
      <h1 style={{ textAlign: "center" }}>
        {" "}
        Comparing {org1.name} with {org2.name}{" "}
      </h1>
      <Container style={{ maxWidth: "80%" }}>
        <Row>
          <Col>{RenderOrg(org1, animals1)}</Col>
          <Col>{RenderOrg(org2, animals2)}</Col>
        </Row>
      </Container>
    </React.Fragment>
  );
}

class OrgCompare extends React.Component {
  render() {
    return (
      <div>
        <MainNav />
        <OrgData />
      </div>
    );
  }
}

export default OrgCompare;
