import React from "react";
import { Card, Figure } from "react-bootstrap";
import LoadAction from "react-loading";
import "../bootstrap.css";
import "./org.css";

const style = {
  spinner: {
    fill: "white",
    width: "10%",
    height: "10%",
  },
};

function RenderOrg(org, animals) {
  // Loading screen
  if (org.name === "name")
    return (
      <LoadAction
        className="load-spinner"
        type="spinningBubbles"
        style={style.spinner}
        color="white"
      />
    );
  // Create linking element for country
  let countryLink = org.country;
  if (countryLink !== null)
    countryLink = (
      <React.Fragment>
        <a href={"/countries/" + encodeURI(org.country)}> {org.country} </a>
        <img
          style={{ maxHeight: "30px", width: "auto", marginLeft: "10px" }}
          src={org.country_flag}
          alt="flag"
        ></img>
      </React.Fragment>
    );

  // Create list of animals in organization
  animals.sort((a, b) => (a.name > b.name ? 1 : -1));
  let animalList = animals.map((animal) => (
    <div>
      <a href={"/animals/" + animal.name}> {animal.name} </a>
    </div>
  ));
  if (animals.length === 0) animalList = <span>None</span>;
  // Return formatted elements
  return (
    <React.Fragment>
      <h2 style={{ textAlign: "center" }}> {org.name}</h2>
      <img
        className="center-img"
        style={{ marginTop: "1%", marginBottom: "1%" }}
        alt={"Picture of " + org.name}
        src={org.secondary_image}
      />
      <Card className="org-card" variant="outlined">
        <Card.Header>
          <div className="org-header">
            <Figure>
              <Figure.Image
                className="center-img"
                alt="Logo"
                src={org.primary_image}
              />
              <Figure.Caption style={{ textAlign: "center" }}>
                Logo of {org.name}
              </Figure.Caption>
            </Figure>
          </div>
          <div className="org-header">
            <Card.Text>Country: {countryLink}</Card.Text>
            <Card.Text>Founding Year: {org.year}</Card.Text>
            <Card.Text>
              Website: <a href={org.website}>{org.website}</a>
            </Card.Text>
          </div>
        </Card.Header>
      </Card>
      <Card
        style={{
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "1%",
          maxWidth: "75%",
        }}
      >
        <Card.Body>
          <Card.Text>{org.description}</Card.Text>
        </Card.Body>
      </Card>
      <Card
        style={{
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "1%",
          maxWidth: "75%",
        }}
      >
        <Card.Body>
          <h3 style={{ textAlign: "center" }}>
            Animals supported by {org.name}
          </h3>
          {animalList}
        </Card.Body>
      </Card>
    </React.Fragment>
  );
}
export default RenderOrg;
