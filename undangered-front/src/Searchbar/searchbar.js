import React, { useState } from "react";
import { Form, Button, InputGroup } from "react-bootstrap";
import "../bootstrap.css";
import { useSpring, a } from "react-spring";

/* 
Renders a searchbar
*/
function Searchbar(props) {
  const [hover, setHover] = useState(false);
  const [value, setValue] = useState("");
  const spring = useSpring({
    width: hover ? 200 : props.animated ? 0 : 200,
  });

  function handleSubmit(event) {
    props.onSearch(value);
    event.preventDefault();
  }
  return (
    <React.Fragment>
      <Form
        style={{ width: "250px", display: "inline-block" }}
        onMouseEnter={(event) => setHover(true)}
        onMouseLeave={(event) => setHover(false)}
        onSubmit={handleSubmit}
      >
        <InputGroup>
          <a.div style={spring}>
            <Form.Control
              type="text"
              value={value}
              placeholder="Search"
              onChange={(event) => setValue(event.target.value)}
            />
          </a.div>
          <InputGroup.Append>
            <Button
              variant="secondary"
              type="submit"
              style={props.green ? { backgroundColor: "#00bc8c" } : {}}
            >
              <img
                style={{ height: "20px", width: "20px" }}
                alt="Search"
                src="https://www.clker.com/cliparts/n/U/H/1/H/u/search-icon-white-one-md.png"
              />
            </Button>
          </InputGroup.Append>
          <div />
        </InputGroup>
      </Form>
    </React.Fragment>
  );
}

export default Searchbar;
