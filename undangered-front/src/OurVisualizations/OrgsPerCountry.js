import React from 'react';
import axios from "axios";
import {
    PieChart, Pie, Tooltip, Cell
} from 'recharts';

class OrgsPerCountry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          data: {
              data01: null,
              data02: null,
          }
        };
    }

    componentDidMount() {
        const result = {}
        const consumable = []
        axios.get("https://api.undangered.ml/api/animalSearch")
            .then((req) => {return req.data})
            .then((data) => {
                data.forEach(element => {
                    let curr = element.status.split(" ")[0]
                    if(result[curr]) {
                        result[curr] += 1
                    }
                    else{
                        result[curr] = 1
                    }
                });
                return result
            })
            .then((data) => {
                for (const [key, value] of Object.entries(data)){
                    consumable.push({name: key, value: value})
                }
                return consumable
            })
            .then((data => {                
                this.setState({data: {
                    data01: data
                }})
            }))
    }

    render() {
        const styles = {
            container: {
              display: "grid",
              justifyItems: "center"
            }
        };
        return (
            <div ref="chart" style={styles.container}>
                <h2 style={{ textAlign: "center" }}>Number of Species Endangered, Threatened, or Extinct</h2>
                <p style={{ textAlign: "center" }}>A color coded look at the number of animals in our database that are Endangered (red), Threatened (green), and Extinct (gray)</p>
                <PieChart width={1000} height={500}>
                    <Pie dataKey="value" data={this.state.data.data01} label>
                        <Cell key={`cell`} fill={"#f5428a"}/>
                        <Cell key={`cell`} fill={"#40f56d"}/>
                    </Pie>
                    <Tooltip />
                </PieChart>
            </div>
        );
    }
}

export default OrgsPerCountry
