import WorldMap from "./WorldMap";
import React from 'react';
import axios from "axios";

class EndangeredByCountry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          data: {
            speciesData: null,
            geoData: null
          }
        };
    }

    componentDidMount() {
        Promise.all([axios.get("https://api.undangered.ml/api/animalCountByCountry"), 
            axios.get("https://raw.githubusercontent.com/ahebwa49/geo_mapping/master/src/world_countries.json")])
        .then(([data, geoData]) => {return [data.data, geoData.data]})
        .then(([numOfEndangeredByCountry, geoData]) => {
            this.setState({data: {
                speciesData: numOfEndangeredByCountry,
                geoData: geoData
            }})
        })
    }

    render() {
        const { data } = this.state;
        return <div>{data.geoData && <WorldMap data={data} />}</div>;
    }
}
export default EndangeredByCountry
