import React from "react";
import * as d3Legend from "d3-svg-legend";
import * as d3 from "d3";
import * as d3Scale from "d3-scale-chromatic";
const margin = 75;
const width = 1200 - margin;
const height = 650 - margin;
class WorldMap extends React.Component {
  componentDidMount() {
    const { data } = this.props;
    const svg = d3
      .select(this.refs.chart)
      .append("svg")
      .attr("width", width)
      .attr("height", height)
      .append("g");

    svg.append("rect")
      .attr("width", "100%")
      .attr("height", "100%")
      .attr("fill", "pink");
    
    // Legend
    var colorScheme = d3Scale.schemeBlues[6];
    colorScheme.unshift("#eee")
    var colorScale = d3.scaleThreshold()
        .domain([1, 11, 21, 31, 41, 51])
        .range(colorScheme);
    var g = svg.append("g")
        .attr("class", "legendThreshold")
        .attr("transform", "translate(20,20)");
    var labels = ['0', '1-5', '6-10', '11-20', '21-30', '31-40', '> 40'];
    var legend = d3Legend.legendColor()
        .labels(function (d) { return labels[d.i]; })
        .shapePadding(4)
        .scale(colorScale);
    svg.select(".legendThreshold")
        .call(legend);

        // Map
    const projection = d3
        .geoMercator()
        .scale(130)
        .translate([width / 2, height / 1.4]);
    const path = d3.geoPath().projection(projection);
    const map = svg
        .selectAll("path")
        .data(data.geoData.features)
        .enter()
        .append("path")
        .attr("fill", function (d){
          // Pull data for this country
          d.total = data.speciesData[d.properties.name] || 0;
          // Set the color
          return colorScale(d.total);
      })
        .attr("d", path)
        .style("stroke", "black")
        .style("stroke-width", 0.5);
    }
    render() {
      const styles = {
        container: {
          display: "grid",
          justifyItems: "center"
        }
      };
    return (
         <div ref="chart" style={styles.container}>
           <h2 style={{ textAlign: "center" }}>Number of Endangered Species By Country</h2>
           <p style={{ textAlign: "center" }}>A visualization of the distribution of endangered animals across the world, organized by country</p>
         </div>
        );
      }
    }
    
export default WorldMap;
