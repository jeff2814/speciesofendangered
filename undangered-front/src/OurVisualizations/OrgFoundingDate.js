import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip} from 'recharts';
import * as React from 'react';
import * as axios from 'axios';

class OrgFoundingDate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          data: {
              data01: null
          }
        };
    }

    componentDidMount() {
        const result = {}
        const consumable = []
        axios.get("https://api.undangered.ml/api/organizationSearch")
            .then((req) => {return req.data})
            .then((data) => {
                data.forEach((ele) => {
                    if(result[ele['year']]){
                        result[ele['year']] += 1
                    }
                    else{
                        result[ele['year']] = 1   
                    }
                })
                return result
            })
            .then((data) => {
                const keys = Object.keys(data);
                keys.sort()
                let start = parseInt(keys[0])
                let stop = parseInt(keys[keys.length - 2])
                while(start < stop){
                    let count = 0
                    var i = 0
                    for(i = 0; i < 10; i++){
                        count += data[start + i] || 0
                    }
                    consumable.push({name: start - 10 + "-" + start, v: count})
                    start += 10
                }
                return consumable
            })
            .then((data => {               
                this.setState({data: {
                    data01: data
                }})
            }))
    }
	render () {
        const styles = {
            container: {
              display: "grid",
              justifyItems: "center"
            }
          };
        return (
            <div ref="chart" style={styles.container}>
                <h2 style={{ textAlign: "center" }}>Histogram of Organization Founding Dates</h2>
                <p style={{ textAlign: "center" }}>A view of when the organizations we support were started</p>
                <BarChart
                    width={1000}
                    height={300}
                    data={this.state.data.data01}
                    margin={{
                    top: 5, right: 30, left: 20, bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Bar dataKey="v" fill="#82ca9d" />
                </BarChart>
            </div>
        );
  }
}

export default OrgFoundingDate;