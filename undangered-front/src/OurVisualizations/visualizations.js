import React, { PureComponent } from 'react';
import MainNav from "../MainNav/mainNav.js";
import EndangeredByCountry from "./EndangeredByCountry.js";
import OrgsPerCountry from './OrgsPerCountry.js';
import OrgFoundingDate from './OrgFoundingDate';

class OurVisualizations extends PureComponent {

  render() {
    return (
        <React.Fragment>
            <MainNav />
            <div className="title">
               <h1>Undangered Visualizations</h1>
            </div>
            <EndangeredByCountry/>
            <OrgsPerCountry/>
            <OrgFoundingDate/>
        </React.Fragment>
    );
  }
}


export default OurVisualizations;
