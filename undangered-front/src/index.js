import React from "react";
import ReactDOM from "react-dom";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";

import Home from "./Home/home.js";
import Animal from "./Animal/animal.js";
import AnimalInstance from "./Animal/animalInstance.js";
import AnimalCompare from "./Animal/animalCompare.js";
import Org from "./Org/org.js";
import OrgInstance from "./Org/orgInstance.js";
import OrgCompare from "./Org/orgCompare.js";
import Country from "./Country/country.js";
import CountryInstance from "./Country/countryInstance.js";
import CountryCompare from "./Country/countryCompare.js";
import Results from "./SearchResults/searchResults.js";
import About from "./About/about.js";
import OurVisualizations from "./OurVisualizations/visualizations.js"
import PoliticianVisualizations from "./PoliticianVisualizations/Visualizations.js"
import "./index.css";

const routing = (
   <React.Fragment>
      <Router>
         <div>
            <Switch>
               <Route exact path="/" component={Home} />
               <Route exact path="/animals" component={Animal} />
               <Route
                  exact
                  path="/animals/:animal"
                  component={AnimalInstance}
               />
               <Route
                  exact
                  path="/animals/compare/:animalsRaw"
                  component={AnimalCompare}
               />
               <Route exact path="/organizations" component={Org} />
               <Route
                  exact
                  path="/organizations/compare/:organizationsRaw"
                  component={OrgCompare}
               />
               <Route
                  exact
                  path="/organizations/:organization"
                  component={OrgInstance}
               />
               <Route exact path="/countries" component={Country} />
               <Route
                  exact
                  path="/countries/:country"
                  component={CountryInstance}
               />
               <Route
                  exact
                  path="/countries/compare/:countriesRaw"
                  component={CountryCompare}
               />
               <Route exact path="/about" component={About} />
               <Route exact path="/OurVisualizations" component={OurVisualizations} />
               <Route exact path="/PoliticianVisualizations" component={PoliticianVisualizations} />
               <Route exact path="/search/:term" component={Results} />
            </Switch>
         </div>
      </Router>
   </React.Fragment>
);

ReactDOM.render(routing, document.getElementById("root"));
