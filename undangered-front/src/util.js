import axios from "axios";

/*  
Make an axios get request at the specified url
*/
function getData(setData, url) {
  axios
    .get(url)
    .then((res) => {
      setData(res.data);
    })
    .catch((err) => {
      setData({});
    });
}

/*  
Filters the input data and returns only objects that have a field which has a
has a value that matches the keyword
*/
function filterData(data, field, filt) {
  let tempData = data;

  for (let i = 0; i < field.length; i++) {
    if (filt[i] !== "" && filt[i] !== "Select All") {
      const filtered = tempData.filter(
        (instance) => instance[field[i]] === filt[i]
      );
      tempData = filtered;
    }
  }

  return tempData;
}

export default getData;
export { filterData };
