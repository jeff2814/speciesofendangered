import React, { PureComponent } from 'react';
import MainNav from "../MainNav/mainNav.js";
import PolByState from './NumOfPolByState.js';
import LengthWords from './LengthWords.js'
import PoliticansPerParty from './PieChartParty.js';

class PoliticianVisualizations extends PureComponent {

  render() {
    return (
        <React.Fragment>
            <MainNav />
            <div className="title">
               <h1>Politician Visualizations</h1>
            </div>
            <PolByState/>
            <LengthWords/>
            <PoliticansPerParty/>
        </React.Fragment>
    );
  }
}


export default PoliticianVisualizations;
