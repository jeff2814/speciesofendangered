import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip} from 'recharts';
import * as React from 'react';
import * as axios from 'axios';

class LengthWords extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          data: {
              data01: null
          }
        };
    }

    componentDidMount() {
        const result = {}
        const consumable = []
        axios.get("https://api.thepolitician.me/policies")
            .then((req) => {return req.data['policies']})
            .then((data) => {
                data.forEach((ele) => {
                    const parse = ele['description'].split(" ")
                    parse.forEach((word) => {
                        if(result[word.length]){
                            result[word.length] += 1
                        }
                        else{
                            result[word.length] = 1   
                        }
                    })
                })
                return result
            })
            .then((data) => {
                for(let key in Object.keys(data)){
                    consumable.push({name: key, v: data[key]})
                }
                return consumable
            })
            .then((data => {              
                this.setState({data: {
                    data01: data
                }})
            }))
    }
	render () {
        const styles = {
            container: {
              display: "grid",
              justifyItems: "center"
            }
          };
        return (
            <div ref="chart" style={styles.container}>
                <h2 style={{ textAlign: "center" }}>Histogram of Policy Word Lengths</h2>
                <p style={{ textAlign: "center" }}>A while back, some researchers did an analysis on the reading level of candidate vocabulary. 
                We decided to take a look at the distribution of word length across all politicians.</p>
                <BarChart
                    width={1000}
                    height={300}
                    data={this.state.data.data01}
                    margin={{
                    top: 5, right: 30, left: 20, bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Bar dataKey="v" fill="#82ca9d" />
                </BarChart>
            </div>
        );
  }
}

export default LengthWords;