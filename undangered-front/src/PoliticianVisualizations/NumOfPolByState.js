import React from "react";
import * as d3Legend from "d3-svg-legend";
import * as d3 from "d3";
import * as d3Scale from "d3-scale-chromatic";
import * as d3Geo from "d3-geo";

import * as topojson from "topojson-client";
import us from "./us.json";
import state_ids from "./name.json";

import Axios from "axios";
const margin = 75;
const width = 1000 - margin;
const height = 650 - margin;

const states = topojson.feature(us, us.objects.states);
const counts = {}

class PolByState extends React.Component {
  componentDidMount() {
    Axios.get("https://api.thepolitician.me/politicians")
    .then((res) => {return res.data['politicians']})
    .then((data) => {
        data.forEach((pol) => {
            let state = String(pol.state.split("-")[0])
            if(counts[state]){
                counts[state] += 1
            }
            else{
                counts[state] = 1
            }
        })
        return counts
    })
    .then((data) => {
        this.setState({data: counts})
    })
    
    }
    render() {
        const svg = d3
        .select(this.refs.chart)
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g");
    
      svg.append("rect")
        .attr("width", "100%")
        .attr("height", "100%")
        .attr("fill", "pink");
      
      // Legend
      var colorScheme = d3Scale.schemeBlues[5];
      colorScheme.unshift("#eee")
      var colorScale = d3.scaleThreshold()
          .domain([1, 6, 11, 16, 21, 26])
          .range(colorScheme);
      var g = svg.append("g")
          .attr("class", "legendThreshold")
          .attr("transform", "translate(20,20)");
      var labels = ['0', '1-5', '6-10', '11-15', '16-20', '21-25', '> 26'];
      var legend = d3Legend.legendColor()
          .labels(function (d) { return labels[d.i]; })
          .shapePadding(4)
          .scale(colorScale);
      svg.select(".legendThreshold")
          .call(legend);
  
          // Map
      const projection = d3.geoAlbersUsa()
          .translate([width / 2, height / 1.4]);
  
      const path = d3.geoPath(d3Geo.geoAlbersUsa());
  
      const map = svg
          .selectAll("path")
          .data(states.features) 
          .enter()
          .append("path")
          .attr("fill", function (d){
            d.total = counts[state_ids[d.id]['state_id']];
            return colorScale(d.total);
        })
          .attr("d", path)
          .style("stroke", "black")
          .style("stroke-width", 0.5);
      const styles = {
        container: {
          display: "grid",
          justifyItems: "center"
        }
      };
      return (
         <div ref="chart" style={styles.container}>
           <h2 style={{ textAlign: "center" }}>Number of Polititians by State</h2>
           <p style={{ textAlign: "center" }}>A visualization of which states produce the most politicians</p>
         </div>
        );
      }
    }
    
export default PolByState;
