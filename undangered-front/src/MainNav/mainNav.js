import React from "react";
import { Nav, Navbar } from "react-bootstrap";

import "../bootstrap.css";
import "./mainNav.css";
import Searchbar from "../Searchbar/searchbar.js";
import logo from "../assets/images/logo_title.png";

const style = {
  nav_text: {
    color: "white",
    ":hover": {
      border: "1px solid black",
      backgroundColor: "white",
    },
  },
  navbar: {
    width: "100%",
    justifyContent: "space-around",
    display: "flex",
  },
};

/* 
Renders the navbar that is displayed at the top of all pages. 
*/
class MainNav extends React.Component {
  redirectSearch(value) {
    if (value !== "") {
      window.location.href = "/search/" + value;
    }
  }
  render() {
    return (
      <div style={{ marginBottom: "2%" }}>
        <Navbar
          color="white"
          className="main-navbar"
          bg="transparent"
          expand="lg"
          variant="dark"
          style={style.navbar}
        >
          <Navbar.Brand href="/">
            <img
              src={logo}
              width="130"
              height="30"
              className="d-inline-block align-top"
              alt="Undangered logo"
            />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="main-nav" style={style.navbar}>
              <Nav.Link href="/" id="home-button" style={style.nav_text}>
                Home
              </Nav.Link>
              <Nav.Link
                href="/animals"
                id="animals-button"
                style={style.nav_text}
              >
                Animals
              </Nav.Link>
              <Nav.Link
                href="/organizations"
                id="orgs-button"
                style={style.nav_text}
              >
                Organizations
              </Nav.Link>
              <Nav.Link
                href="/countries"
                id="countries-button"
                style={style.nav_text}
              >
                Countries
              </Nav.Link>
              <Nav.Link href="/about" id="about-button" style={style.nav_text}>
                About
              </Nav.Link>
              <Nav.Link href="/OurVisualizations" id="about-button" style={style.nav_text}>
                Our Visuals
              </Nav.Link>
              <Nav.Link href="/PoliticianVisualizations" id="about-button" style={style.nav_text}>
                Provider Visuals
              </Nav.Link>
              <div>
                <Searchbar
                  animated={true}
                  green
                  onSearch={this.redirectSearch}
                />
              </div>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}

export default MainNav;
