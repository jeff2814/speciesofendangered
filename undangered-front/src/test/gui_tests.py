from selenium import webdriver
from unittest import main, TestCase
import warnings


class GUITests(TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path="./chromedriver")
        warnings.filterwarnings(
            action="ignore", message="unclosed", category=ResourceWarning
        )

    # Online testing
    def test_animals_page(self):
        self.driver.get("https://www.undangered.ml/")
        art_button = self.driver.find_element_by_id("animals-button")
        art_button.click()
        self.assertIn("https://www.undangered.ml/animals", self.driver.current_url)
        self.driver.close()

    def test_orgs_page(self):
        self.driver.get("https://www.undangered.ml/")
        artists_button = self.driver.find_element_by_id("orgs-button")
        artists_button.click()
        self.assertIn(
            "https://www.undangered.ml/organizations", self.driver.current_url
        )
        self.driver.close()

    def test_countries_page(self):
        self.driver.get("https://www.undangered.ml/")
        museums_button = self.driver.find_element_by_id("countries-button")
        museums_button.click()
        self.assertIn("https://www.undangered.ml/countries", self.driver.current_url)
        self.driver.close()

    def test_about_page(self):
        self.driver.get("https://www.undangered.ml/")
        art_button = self.driver.find_element_by_id("about-button")
        art_button.click()
        self.assertIn("https://www.undangered.ml/about", self.driver.current_url)
        self.driver.close()

    def test_navbar(self):
        self.driver.get("https://www.undangered.ml/")
        nav_art = self.driver.find_element_by_id("animals-button")
        nav_art.click()
        self.assertIn("https://www.undangered.ml/animals", self.driver.current_url)
        nav_artists = self.driver.find_element_by_id("orgs-button")
        nav_artists.click()
        self.assertIn(
            "https://www.undangered.ml/organizations", self.driver.current_url
        )
        nav_museums = self.driver.find_element_by_id("countries-button")
        nav_museums.click()
        self.assertIn("https://www.undangered.ml/countries", self.driver.current_url)
        nav_about = self.driver.find_element_by_id("about-button")
        nav_about.click()
        self.assertIn("https://www.undangered.ml/about", self.driver.current_url)
        nav_homepage = self.driver.find_element_by_id("home-button")
        nav_homepage.click()
        self.assertIn("https://www.undangered.ml/", self.driver.current_url)


if __name__ == "__main__":  # pragma: no cover
    main()
