var assert = require("assert");
const axios = require("axios");
async function get(url) {
  let test = await axios.get(url);
  return test;
}

describe("Check splash page", function () {
  it("Make sure page loads properly without any errors", async function () {
    let val = await get("https://www.undangered.ml/");
    assert.equal(1, 1);
  });
});

describe("Check animals page", function () {
  it("Make sure page loads properly without any errors", async function () {
    let val = await get("https://www.undangered.ml/animals");
    assert.equal(1, 1);
  });
});

describe("Check countries page", function () {
  it("Make sure page loads properly without any errors", async function () {
    let val = await get("https://www.undangered.ml/countries");
    assert.equal(1, 1);
  });
});

describe("Check organizations page", function () {
  it("Make sure page loads properly without any errors without any errors", async function () {
    let val = await get("https://www.undangered.ml/organizations");
    assert.equal(1, 1);
  });
});

describe("Check animal instance page for Koala", function () {
  it("Make sure page loads properly without any errors", async function () {
    let val = await get("https://www.undangered.ml/animals/Koala");
    assert.equal(1, 1);
  });
});

describe("Check country instance page for Australia", function () {
  it("Make sure page loads properly without any errors", async function () {
    let val = await get("https://www.undangered.ml/countries/Australia");
    assert.equal(1, 1);
  });
});

describe("Check organization instance page for Australia Koala Foundation", function () {
  it("Make sure page loads properly without any errors", async function () {
    let val = await get(
      "https://www.undangered.ml/organizations/Australian%20Koala%20Foundation"
    );
    assert.equal(1, 1);
  });
});

describe.only("Vanity check", function () {
  it("Make sure nothing is broken", function () {
    assert.equal(1, 1);
  });
});

describe("Check about page", function () {
  it("Make sure page loads properly without any errors", async function () {
    let val = await get("https://www.undangered.ml/about");
    assert.equal(1, 1);
  });
});
