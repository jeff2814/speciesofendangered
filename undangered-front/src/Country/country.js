import React, { useEffect, useState } from "react";
import axios from "axios";
import DataTable from "react-data-table-component";
import LoadAction from "react-loading";
import SelectSearch from "react-select-search";
import { Button } from "react-bootstrap";

import Searchbar from "../Searchbar/searchbar.js";
import MainNav from "../MainNav/mainNav.js";
import { filterData } from "../util.js";
import tableCols from "./tableCols.js";
import regionList from "./regionList.js";

import "../bootstrap.css";
import "./country.css";
import "../selectSearch.css";

const style = {
  spinner: {
    fill: "white",
    width: "10%",
    height: "10%",
  },
};

/* 
Makes an axios request to update the data for the table
*/
function fillData(setLoading, setData) {
  setLoading(true);

  axios
    .get("https://api.undangered.ml/api/nationSearch/")
    .then((res) => {
      setData(res.data);
      setLoading(false);
    })
    .catch((err) => {
      setData([]);
      setLoading(false);
    });
}

/*
Generates the table displaying the organization table
Returns:
1) Filters
2a) a table displaying the organization table if data has finished loading
2b) a spinner if the data has not finished loading
*/
function CountryTable(props) {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [curData, setCurData] = useState([]);
  const [filter1, setFilter1] = useState("");
  const [search, setSearch] = useState("");
  const [selected, setSelected] = useState([]);

  useEffect(() => {
    fillData(setLoading, setData);
  }, []);

  useEffect(() => {
    setCurData(filterData(data, ["region"], [filter1]));
  }, [data, filter1]);

  let searchData = curData;

  // Filter out the search terms
  if (search !== "") {
    searchData = searchData.filter((model) => {
      for (var key in model) {
        if (key === "primary_image" || key === "secondary_image") continue;
        if (
          JSON.stringify(model[key])
            .toLowerCase()
            .indexOf(search.toLowerCase()) >= 0
        )
          return true;
      }
      return false;
    });
  }

  function handleChange(state) {
    setSelected(state.selectedRows);
  }
  function compareSelected() {
    if (selected.length !== 2) {
      alert("Please Select exactly 2 instances to compare.");
    } else {
      let countries = selected.reduce((a, b) => a.name + "bigboi" + b.name);
      window.location.href = "/countries/compare/" + countries;
    }
  }
  let inline = {
    display: "inline-block",
    position: "relative",
    float: "right",
    marginRight: "20px",
  };

  return !loading ? (
    <React.Fragment>
      <div className="country-filter-dropdowns" id="countries-filters">
        <h5 className="country-filter-header">Filters:</h5>
        <SelectSearch
          options={regionList}
          onChange={setFilter1}
          search
          placeholder="Select a region"
        />

        <Button style={inline} variant="secondary" onClick={compareSelected}>
          Compare Selected
        </Button>
        <Searchbar onSearch={(value) => setSearch(value)} />
      </div>
      <div className="table-div" id="countries-table-div">
        <DataTable
          columns={tableCols(search)}
          data={searchData}
          pagination
          compact
          striped
          selectableRows
          onSelectedRowsChange={handleChange}
          highlightOnHover
          pointerOnHover
          persistTableHead
        />
      </div>
    </React.Fragment>
  ) : (
    <div className="table-div" id="countries-table-div">
      <LoadAction
        className="load-spinner"
        type="spinningBubbles"
        style={style.spinner}
        color="white"
      />
    </div>
  );
}

class Country extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };
  }
  render() {
    return (
      <React.Fragment>
        <MainNav />
        <h1 className="title" id="countries-title">
          Countries
        </h1>
        <div
          style={{
            textAlign: "right",
            margin: "0",
            paddingRight: "5%",
          }}
        ></div>
        <CountryTable search={this.state.value} />
      </React.Fragment>
    );
  }
}

export default Country;
