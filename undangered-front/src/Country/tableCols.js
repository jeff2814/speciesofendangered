import React from "react";
import Highlight from "react-highlighter";

/*
Returns the columns for the Country model table
*/
function tableCols(searchTerm) {
  const cols = [
    {
      name: "Country",
      selector: "name",
      sortable: true,
      cell: (row) => (
        <a href={"/countries/" + encodeURI(row.name)}>
          {" "}
          <Highlight search={searchTerm}>{row.name}</Highlight>
        </a>
      ),
    },
    {
      name: "Capital City",
      selector: "capital",
      sortable: true,
      cell: (row) => <Highlight search={searchTerm}>{row.capital}</Highlight>,
    },
    {
      name: "Region",
      selector: "region",
      sortable: true,
      cell: (row) => <Highlight search={searchTerm}>{row.region}</Highlight>,
    },
    {
      name: "Flag",
      selector: "primary_image",
      sortable: false,
      cell: (row) => <img alt="Country Flag" src={row.primary_image} />,
    },
    {
      name: "Map",
      selector: "secondary_image",
      sortable: true,
      cell: (row) => <img alt="Region Map" src={row.secondary_image} />,
    },
  ];
  return cols;
}

export default tableCols;
