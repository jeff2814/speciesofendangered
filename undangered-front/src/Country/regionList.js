const regionList = [
  { name: "Select All", value: "Select All" },
  { name: "East Asia & Pacific", value: "East Asia & Pacific" },
  { name: "Europe & Central Asia", value: "Europe & Central Asia" },
  { name: "Latin America & Caribbean", value: "Latin America & Caribbean " },
  { name: "Middle East & North Africa", value: "Middle East & North Africa" },
  { name: "North America", value: "North America" },
  { name: "South Asia", value: "South Asia" },
  { name: "Sub-Saharan Africa", value: "Sub-Saharan Africa " },
];

export default regionList;
