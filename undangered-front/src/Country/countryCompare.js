import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";

import MainNav from "../MainNav/mainNav.js";
import RenderCountry from "./renderCountry.js";
import getData from "../util.js";
import "../bootstrap.css";
import "./country.css";

function CountryData() {
  let def = {
    id: "id",
    name: "name",
    capital: "capital",
    region: "region",
    latitude: "latitude",
    longitude: "longitude",
    organization: "organization",
  };

  // Get country data
  let { countriesRaw } = useParams();
  let countries = countriesRaw.split("bigboi");
  let name1 = countries[0];
  let name2 = countries[1];
  const [country1, setCountry1] = useState(def);
  const [animals1, setAnimals1] = useState([]);
  const [orgs1, setOrgs1] = useState([]);
  const [country2, setCountry2] = useState(def);
  const [animals2, setAnimals2] = useState([]);
  const [orgs2, setOrgs2] = useState([]);

  useEffect(() => {
    getData(setCountry1, "https://api.undangered.ml/api/nationInfo/" + name1);
    getData(
      setAnimals1,
      "https://api.undangered.ml/api/animalsByCountry/" + encodeURI(name1)
    );
    getData(
      setOrgs1,
      "https://api.undangered.ml/api/organizationsByCountry/" + encodeURI(name1)
    );
  }, [name1]);

  useEffect(() => {
    getData(setCountry2, "https://api.undangered.ml/api/nationInfo/" + name2);
    getData(
      setAnimals2,
      "https://api.undangered.ml/api/animalsByCountry/" + encodeURI(name2)
    );
    getData(
      setOrgs2,
      "https://api.undangered.ml/api/organizationsByCountry/" + encodeURI(name2)
    );
  }, [name2]);

  return (
    <React.Fragment>
      <h1 style={{ textAlign: "center" }}>
        {" "}
        Comparing {country1.name} with {country2.name}{" "}
      </h1>
      <Container style={{ maxWidth: "50%" }}>
        <Row>
          <Col>{RenderCountry(country1, animals1, orgs1)}</Col>
          <Col>{RenderCountry(country2, animals2, orgs2)}</Col>
        </Row>
      </Container>
    </React.Fragment>
  );
}

class CountryCompare extends React.Component {
  render() {
    return (
      <React.Fragment>
        <MainNav />
        <CountryData />
      </React.Fragment>
    );
  }
}

export default CountryCompare;
