import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import MainNav from "../MainNav/mainNav.js";
import RenderCountry from "./renderCountry.js";
import getData from "../util.js";
import "../bootstrap.css";
import "./country.css";

function CountryData() {
  let def = {
    id: "id",
    name: "name",
    capital: "capital",
    region: "region",
    latitude: "latitude",
    longitude: "longitude",
    organization: "organization",
  };

  // Get country data
  let { country } = useParams();
  const [countryJSON, setCountryJSON] = useState(def);
  const [animals, setAnimals] = useState([]);
  const [orgs, setOrgs] = useState([]);

  useEffect(() => {
    let countryName = country;
    getData(
      setCountryJSON,
      "https://api.undangered.ml/api/nationInfo/" + countryName
    );
    getData(
      setAnimals,
      "https://api.undangered.ml/api/animalsByCountry/" + encodeURI(countryName)
    );
    getData(
      setOrgs,
      "https://api.undangered.ml/api/organizationsByCountry/" +
        encodeURI(countryName)
    );
  }, [country]);

  return RenderCountry(countryJSON, animals, orgs);
}

class CountryInstance extends React.Component {
  render() {
    return (
      <React.Fragment>
        <MainNav />
        <CountryData />
      </React.Fragment>
    );
  }
}

export default CountryInstance;
