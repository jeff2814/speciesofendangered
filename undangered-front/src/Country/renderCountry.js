import React from "react";
import { Card, Figure } from "react-bootstrap";
import LoadAction from "react-loading";

const style = {
  spinner: {
    fill: "white",
    width: "10%",
    height: "10%",
  },
};

function RenderCountry(countryJSON, animals, orgs) {
  // Loading screen
  if (countryJSON.name === "name")
    return (
      <LoadAction
        className="load-spinner"
        type="spinningBubbles"
        style={style.spinner}
        color="white"
      />
    );
  // Create list of animals in country
  animals.sort((a, b) => (a.name > b.name ? 1 : -1));
  let animalList = animals.map((animal) => (
    <div key={animal.id}>
      <a href={"/animals/" + animal.name}> {animal.name} </a>
    </div>
  ));

  // Create list of orgs in country
  orgs.sort((a, b) => (a.name > b.name ? 1 : -1));
  let orgList = orgs.map((org) => (
    <div key={org.name}>
      <a href={"/organizations/" + org.name}> {org.name} </a>
      <img
        style={{ maxHeight: "30px", width: "auto", marginLeft: "10px" }}
        src={org.secondary_image}
        alt="flag"
      ></img>
    </div>
  ));
  if (animals.length === 0) animalList = <span>None</span>;
  if (orgs.length === 0) orgList = <span>None</span>;
  return (
    <React.Fragment>
      <h2 style={{ textAlign: "center" }}> {countryJSON.name}</h2>
      <img
        className="center-img"
        style={{ marginTop: "1%", marginBottom: "1%" }}
        alt={"Map of " + countryJSON.name}
        src={countryJSON.secondary_image}
      />
      <Card className="country-card" variant="outlined">
        <Card.Header>
          <div className="country-header">
            <Figure>
              <Figure.Image
                className="center-img"
                alt="Country Flag"
                src={countryJSON.primary_image}
              />
              <Figure.Caption style={{ textAlign: "center" }}>
                Flag of {countryJSON.name}
              </Figure.Caption>
            </Figure>
          </div>
          <div className="country-header">
            <Card.Text>Capital: {countryJSON.capital}</Card.Text>
            <Card.Text>Region: {countryJSON.status}</Card.Text>
            <Card.Text>Latitude: {countryJSON.latitude}</Card.Text>
            <Card.Text>Longitude: {countryJSON.longitude}</Card.Text>
          </div>
        </Card.Header>
      </Card>
      <Card
        style={{
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "1%",
          maxWidth: "75%",
        }}
      >
        <Card.Body>
          <h3 style={{ textAlign: "center" }}>Animals in {countryJSON.name}</h3>
          {animalList}
        </Card.Body>
      </Card>
      <Card
        style={{
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "1%",
          maxWidth: "75%",
        }}
      >
        <Card.Body>
          <h3 style={{ textAlign: "center" }}>
            Organizations in {countryJSON.name}
          </h3>
          {orgList}
        </Card.Body>
      </Card>
    </React.Fragment>
  );
}
export default RenderCountry;
