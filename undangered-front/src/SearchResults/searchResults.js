import React, { useEffect, useState } from "react";
import axios from "axios";
import { ListGroup } from "react-bootstrap";
import LoadAction from "react-loading";
import { useParams } from "react-router-dom";
import { useTrail, a } from "react-spring";
import MainNav from "../MainNav/mainNav.js";
import "../bootstrap.css";

const style = {
  spinner: {
    fill: "white",
    width: "10%",
    height: "10%",
  },
};

/* 
Makes an axios request to update the data for the results
*/
function fillData(setLoading, setData, term) {
  axios
    .get("https://api.undangered.ml/api/search/" + term)
    .then((res) => {
      setData(res.data);
      setLoading(false);
    })
    .catch((err) => {
      setData({});
      setLoading(false);
    });
}

/*
Returns a shortened text snippet from an animal, country, or organization
Snippet will have search terms highlighted/bold
*/
function getSnippet(obj, term) {
  function snippetize(str, searches) {
    let first = searches
      .map(function (s) {
        var occ = str.toLowerCase().indexOf(s);
        occ = occ < 0 ? str.length : occ;
        return occ;
      })
      .reduce((a, b) => (a > b ? b : a));
    first = first === str.length ? 0 : first;
    let startInd = Math.max(0, first - 300);
    let endInd = Math.min(str.length, startInd + 600);
    let prefix = startInd > 0 ? "..." : "";
    let suffix = endInd < str.length ? "..." : "";
    return prefix + str.substring(startInd, endInd) + suffix;
  }
  let ignore = [
    "name",
    "primary_image",
    "secondary_image",
    "country_backup",
    "name_backup",
    "id",
    "country_flag",
    "organization_logo",
    "website",
  ];
  let snippet = Object.keys(obj)
    .map((key, index) => (ignore.indexOf(key) > -1 ? "" : obj[key]))
    .reduce((a, b) => (a && b ? a + " | " + b : a + b), "");

  let terms = term.split(" ").map((s) => s.toLowerCase());
  let result = snippetize(snippet, terms);
  let t = terms.reduce((a, b) => (a && b ? a + "|" + b : a + b), "");
  let exp = new RegExp(`(${t})`, "gi");

  const parts = result.split(exp);
  let i = 0;
  result = parts.map((part) =>
    terms.indexOf(part.toLowerCase()) > -1 ? (
      <span key={"result_" + i++} style={{ backgroundColor: "#00bc8c" }}>
        {part}
      </span>
    ) : (
      part
    )
  );
  return <p>{result}</p>;
}

/*
Generates the list of search results, with a spinner as a placeholder as requests are made
*/
function ResultsList() {
  let { term } = useParams();
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fillData(setLoading, setData, term);
  }, [term]);

  let pathMap = {
    animals: "Animal",
    nations: "Country",
    organizations: "Organization",
  };

  const trail = useTrail(20, {
    config: { mass: 5, tension: 500, friction: 100 },
    opacity: 0.8,
    height: 200,
    from: { opacity: 0, height: 0 },
  });
  //Construct List Item Objects
  let i = 0;
  let results = Object.keys(data).map(function (key, index) {
    let res = data[key].map(function (obj, index2) {
      i++;
      let url = "https://www.undangered.ml/" + key + "/" + obj.name;
      let item = (
        <a.div key={"result_" + i} style={trail[i]}>
          <ListGroup.Item action style={{ height: "100%" }} href={url}>
            <h5 style={{ color: "#00bc8c" }}>{obj.name}</h5>
            <h6>
              <i>{pathMap[key]}</i>
            </h6>
            {getSnippet(obj, term)}
          </ListGroup.Item>
        </a.div>
      );
      return i < 20 ? item : "";
    });
    return res;
  });
  const header = i > 0 ? "Search Results" : "No Results Found";
  return !loading ? (
    <React.Fragment>
      <h1 className="title" id="results-title">
        {header}
      </h1>
      <ListGroup>{results}</ListGroup>
    </React.Fragment>
  ) : (
    <React.Fragment>
      <h1 className="title" id="results-title">
        Search Results
      </h1>
      <LoadAction
        className="load-spinner"
        type="spinningBubbles"
        style={style.spinner}
        color="white"
      />
    </React.Fragment>
  );
}

class Results extends React.Component {
  render() {
    return (
      <React.Fragment>
        <MainNav />
        <div className="table-div" id="results-table-div">
          <ResultsList />
        </div>
      </React.Fragment>
    );
  }
}

export default Results;
