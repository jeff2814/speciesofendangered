# Undangered
Members (format: Name, EID, GitLab ID): 
    Akshay Mantri, am86892, AkshayMantri
    Huy Tran, hdt399, huydtran121
    Daniel Gao, djg3296, 18dgao
    Michael Yang, my5658, ahuei123456
    Jeffery Wu, jw48726, jeff2814

Project Leader: Daniel Gao

Git SHA: 02d0e5049cc69b494c190d20db667d81fb03f7a3
Link to GitLab pipelines: https://gitlab.com/jeff2814/speciesofendangered/-/pipelines
Link to Website: https://www.undangered.ml/

Estimated Completion Times, Actual Completion Times:
    Akshay Mantri: 10, 12
    Huy Tran: 10, 8
    Daniel Gao: 10, 11
    Michael Yang: 10, 7
    Jeffery Wu: 10, 11

Comments: There may be errors or warnings on in the console of our website, but those are due to our visualizations page rather than any of the requirements for this deadline.
