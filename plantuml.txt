@startuml
actor User
participant "Load Balancer"
participant "Frontend Server"
participant "Backend Server"
database "database"
User -> "Load Balancer" : GET Webpage
"Load Balancer" -> "Frontend Server" : GET Webpage
"Frontend Server" -> "Load Balancer" : Webpage
"Load Balancer" -> User : Webpage
User -> "Load Balancer" : GET Data
"Load Balancer" -> "Backend Server" : GET Data
"Backend Server" -> database: Data Query
database -> "Backend Server" : Query Result
"Backend Server" -> "Load Balancer" : Data
"Load Balancer" -> User : Data
@enduml
