

flask:
	cd undangered-back/
	docker build -t undangered-back:latest undangered-back/
	docker run -it -d -p 5000:5000 undangered-back

flask_test:
	cd undangered-back/
	docker build -t undangered-back:latest undangered-back/
	docker run -it -p 5000:5000 undangered-back

back_test:
	pip3 install -r undangered-back/requirements.txt
	cd undangered-back/ && curl http://anchan.dyreatic.moe:1234/db >db.json && curl http://anchan.dyreatic.moe:1234/token >token.json && pytest

flask_dockerless:
	cd undangered-back/ && uwsgi --ini uwsgi.ini

react:
	cd undangered-front/
	docker build -t undangered-front:latest undangered-front/
	docker run -it -d --rm -v /app/node_modules -p 443:443 undangered-front

react_test:
	cd undangered-front/
	docker build -t undangered-front:latest undangered-front/
	docker run -it --rm -v /app/node_modules -p 443:443 undangered-front

front_test:
	cd undangered-front/ && npm install && npm test
